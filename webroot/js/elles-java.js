//Wenn mehrere Charts stehen globals einstellen
//Chart.defaults.global.responsive = true;

Chart.defaults.global.defaultFontFamily = 'Arial';

if (typeof modesChartData !== 'undefined') {

    var modesLabels = [];
    var modesValues = [];

    for(i in modesChartData)
    {
        modesLabels.push(modesChartData[i].m_name);
        modesValues.push(modesChartData[i].count);
    }

    var data = {
        type: 'pie',
        labels: modesLabels,
        datasets: [{
            data: modesValues,
            backgroundColor: [
                'rgba(34, 94, 164, 0.8)',
                'rgba(36, 132, 68, 0.8)'
        ]
        }]
    };

    var options =
        {
            //maintainAspectRatio: false,
            responsive: true
        };

    const contextModesChart = document.getElementById("modesPieChart").getContext("2d");

    var modesPieChart = new Chart(contextModesChart,{
        type: 'pie',
        data: data,
        options: options
    });
}

if (typeof appsChartData !== 'undefined') {

    var appsLabels = [];
    var appsValues = [];

    for(i in appsChartData)
    {
        if(appsChartData[i].origin == '')
        {
            appsLabels.push('ohne App');
        } else {
            appsLabels.push(appsChartData[i].origin);
        }
        appsValues.push(appsChartData[i].count);
    }

    var data = {
        type: 'doughnut',
        labels: appsLabels,
        datasets: [{
            data: appsValues,
            backgroundColor: [
                'rgba(204, 76, 3, 0.8)',
                'rgba(226, 26, 26, 0.8)',
                'rgba(102, 38, 5, 0.8)',
                'rgba(34, 94, 164, 0.8)',
                'rgba(36, 132, 68, 0.8)',
                'rgba(74, 1, 106, 0.8)'
            ]
        }]
    };

    var options =
        {
            //maintainAspectRatio: false,
            responsive: true
        };

    const contextAppsChart = document.getElementById("appsPieChart").getContext("2d");

    var appsPieChart = new Chart(contextAppsChart,{
        type: 'doughnut',
        data: data,
        options: options
    });
}

if (typeof lessonsChartData !== 'undefined') {

    var chartOptions =
        {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Anzahl Übungen'
                    },
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        callback: function(value) {
                            return new Date(value).toLocaleDateString('de-DE', {month:'short'});
                        }
                    },
                    type: 'time',
                    time: {
                        format: "MMM",
                        //unit: 'month',
                        unitStepSize: 1,
                        //round: 'month',
                        //min: dateMin,
                        max: dateMax,
                        //tooltipFormat: 'DD.MMM.YYYY',
                        displayFormats: {
                            //'millisecond': 'MMM DD',
                            //'second': 'MMM DD',
                            //'minute': 'MMM DD',
                            //'hour': 'MMM DD',
                            //'day': 'DD',
                            //'week': 'MMM',
                            'month': 'MMM',
                            //'quarter': 'MMM DD',
                            'year': 'MMM',
                        }
                    },
                    scaleLabel: {
                        display: false
                    },
                    gridLines:{
                        display: false
                    }
                }]
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 10,
                    bottom: 10
                }
            },
            legend: {
                display: false
            },
            tooltips:{
                enabled:false
            }
        };


    var dateMax = new Date();
    dateMax.setDate(dateMax.getDate() + 32);

    //var dateMin = new Date().getFullYear();

    var newDataSet =
        {
            label: "Übungen",
            lineTension: 1,
            fill: true,
            borderWidth: 2,
            backgroundColor: 'rgba(126, 2, 36, 0.8)',
            borderColor: 'rgba(126, 2, 36, 0.8)',
            data: []
        }

    for(i in lessonsChartData)
    {
        var tempData =
            {
                x : new Date(lessonsChartData[i].month + "-01"),
                y : lessonsChartData[i].count
            }

        newDataSet.data.push(tempData);

        console.log(new Date(lessonsChartData[i].month + "-01"));
        console.log(lessonsChartData[i].count);

    }

    const contextLessonsBarChart = document.getElementById("lessonsBarChart");

    var barChart = new Chart(contextLessonsBarChart, {
        type: 'bar',
        data: newDataSet,
        options: chartOptions
    });
}

if (typeof chartDataPerLevel !== 'undefined') {

    var chartdata =
        {
            labels: [''],
            datasets: [ ]
        };

    var dateMax = new Date(lastLessonDate);
    dateMax.setDate(dateMax.getDate() + 32);

    var dateMin = new Date(clientCreated);


    var chartOptions =
    {
        maintainAspectRatio: false,
        responsive: true,
        elements:{
            point: {
                pointStyle: 'circle'
            }
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'in Prozent'
                },
                ticks: {
                    // Y Axes in %
                    //callback: function(value, index, values) {
                    //    return value + ' %';
                    //},
                    beginAtZero:true,
                    suggestedMax: 100
                }
            }],
            xAxes: [{
                ticks: {
                    callback: function(value) {
                        return new Date(value).toLocaleDateString('de-DE', {month:'short'});
                    }
                },
                type: 'time',
                time: {
                    format: "MMM",
                    //unit: 'month',
                    unitStepSize: 1,
                    //round: 'month',
                    min: dateMin,
                    max: dateMax,
                    tooltipFormat: 'DD.MMM.YYYY',
                    displayFormats: {
                        //'millisecond': 'MMM DD',
                        //'second': 'MMM DD',
                        //'minute': 'MMM DD',
                        //'hour': 'MMM DD',
                        //'day': 'DD',
                        //'week': 'MMM',
                        'month': 'MMM',
                        //'quarter': 'MMM DD',
                        'year': 'MMM',
                    }
                },
                scaleLabel: {
                    display: false
                },
                gridLines:{
                    display: true
                }
            }]
        },
        layout: {
            padding: {
                left: 20,
                right: 20,
                top: 10,
                bottom: 10
            }
        },
        legend: {
            display: true,
            position:'right',
            labels: {
                padding: 10,
                fontColor:'#000',
                usePointStyle: true
            }
        },
        tooltips:{
            enabled:true,
            mode: 'single'
            //Versuch exercise_type im Tooltip anzuzeigen
            //callbacks:{
              //  label: function(tooltipItems, data) {

                //    var type = "";

                  //  for(i in lessonType)
                    //{
                      //  if(tooltipItems.xLabel == lessonType[i].date)
                        //{
                          //  type = lessonType[i].et_name;
                        //}
                   // }
                    //return tooltipItems.xLabel + ' €' + type;
               // }
            //}
        }
    };

    //var context = $("#barChart");
    const contextLessonsChart = document.getElementById("barChart");

    var barChart = new Chart(contextLessonsChart, {
        type: 'line',
        data: chartdata,
        options: chartOptions
    });

    //var lessonTypes = [];

    for(j in chartDataPerLevel)
    {
        var newDataSet =
            {
                label: chartDataPerLevel[j][0].c_name.charAt(0) + " Stufe " + chartDataPerLevel[j][0].cl_level,
                lineTension: 0,
                fill: false,
                //showLine: false,
                borderWidth: 2,
                pointRadius: 6,
                pointHoverRadius: 6,
                backgroundColor: get_level_color(chartDataPerLevel[j][0].cl_id)[0],
                borderColor: get_level_color(chartDataPerLevel[j][0].cl_id)[0],
                pointHoverBackgroundColor: get_level_color(chartDataPerLevel[j][0].cl_id)[0],
                pointHoverBorderColor: get_level_color(chartDataPerLevel[j][0].cl_id)[1],
                data: []
            }

        for(i in chartDataPerLevel[j])
        {
            var tmpValue = 0;

            if (chartDataPerLevel[j][i].max_score != 0) {
                tmpValue = Math.round((chartDataPerLevel[j][i].score / chartDataPerLevel[j][i].max_score) * 100);
            }

            var tempData =
                {
                    x : new Date(chartDataPerLevel[j][i].l_date),
                    y : tmpValue
                }
            /*
            var tempData2 =
                {
                    "et_name" : new Date(chartDataPerLevel[j][i].l_date),
                    "date" : chartDataPerLevel[j][i].et_name
                }

            lessonTypes.data.push(tempData2);

            console.log(new Date(chartDataPerLevel[j][i].l_date));
            console.log(new Date(chartDataPerLevel[j][i].et_name));
            */

            newDataSet.data.push(tempData);


        }

        barChart.config.data.datasets.push(newDataSet);
    }

    barChart.update();
}

function get_level_color(level_id) {

    var result = [];
    //result[0] backgroundColor
    //result[1] borderColor
    //result[2] pointHoverBackgroundColor

    if(Number(level_id) <= 6) {
        result[1] = '(226, 26, 26, 1)';
    } else {
        result[1] = 'rgba(32, 94, 170, 1)';
    }

    switch (level_id)
    {
        case "1":     result[0] = 'rgba(255, 215, 119, 0.8)'; break; //Lesen Phon. Stufe 1
        case "2":     result[0] = 'rgba(252, 139, 65, 0.8)'; break; //Lesen Silb. Stufe 2
        case "3":     result[0] = 'rgba(226, 26, 26, 0.8)'; break; //Lesen Silb. Stufe 3
        case "4":     result[0] = 'rgba(126, 2, 36, 0.8)'; break; //Lesen Silb. Stufe 4
        case "5":     result[0] = 'rgba(204, 76, 3, 0.8)'; break; //Lesen Morph. Stufe 5
        case "6":     result[0] = 'rgba(102, 38, 5, 0.8)'; break; //Lesen Synt. Stufe 6
        case "7":     result[0] = 'rgba(198, 235, 178, 0.8)'; break; //Rechtschr. Phon. Stufe 1
        case "8":     result[0] = 'rgba(63, 184, 193, 0.8)'; break; //Rechtschr. Silb. Stufe 2
        case "9":     result[0] = 'rgba(34, 94, 164, 0.8)'; break; //Rechtschr. Silb. Stufe 3
        case "10":    result[0] = 'rgba(8, 29, 82, 1)'; break; //Rechtschr. Silb. Stufe 4
        case "11":    result[0] = 'rgba(122, 197, 123, 0.8)'; break; //Rechtschr. Morph. Stufe 5
        case "12":    result[0] = 'rgba(36, 132, 68, 0.8)'; break; //Rechtschr. Morph. Stufe 6
        case "13":    result[0] = 'rgba(2, 69, 41, 0.8)'; break; //Rechtschr. Morph. Stufe 7
        case "14":    result[0] = 'rgba(74, 1, 106, 0.8)'; break; //Rechtschr. Synt. Stufe 8
        default:    result[0] = 'rgba(0,0,0, 0.5)'; break;
    }
    return result;
}