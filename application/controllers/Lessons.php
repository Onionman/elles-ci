<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lessons extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('users_model');
        $this->load->model('lessons_model');
        $this->load->model('clients_model');
        $this->load->model('user_has_clients_model');
    }

    public function index()
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'lessons';
        $this->load->model('comp_model');
        $this->load->model('charts_model');

        $data['all_exercises'] = $this->lessons_model->get_all_exercises();
        $data['all_exercise_types'] = $this->lessons_model->get_all_exercise_types();
        $data['all_competences'] = $this->comp_model->get_all_competences();
        $data['all_competences_count'] = $this->lessons_model->get_all_competences_count();
        $data['modes_chart_data'] = $this->charts_model->get_modes_chart_data();
        $data['apps_chart_data'] = $this->charts_model->get_apps_chart_data();
        //$data['lessons_chart_data'] = $this->charts_model->get_lessons_per_month_chart_data();

        $this->load->view('lessons/index', $data);
    }

    public function view($lesson_id)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'lessons';

        $curr_user = $this->users_model->get_curr_user();

        $data['lesson'] = $this->lessons_model->get_lesson_by_id($lesson_id);

        // data for component client-profile.php
        $data['client'] = $this->user_has_clients_model->get_relation($curr_user['id'], $lesson_id['client_id']);

        $last_lesson = $this->lessons_model->get_last_lesson($lesson_id['client_id']);
        if(!empty($last_lesson))
        {
            $data['last_lesson'] = $last_lesson;
        }

        $this->load->view('lessons/view', $data);
        //$this->load->view('user/index', $data);
    }

    public function create($client_id)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'lessons';
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        //checks Lesen/Schreiben radiobutton
        if($this->input->post('radios') == 'read')
        {
            $this->form_validation->set_rules('level-select-read', 'Stufe', 'required');
        }
        else
        {
            $this->form_validation->set_rules('level-select-write', 'Stufe', 'required');
        }

        $this->form_validation->set_rules('date-input', 'Datum', 'required');
        $this->form_validation->set_rules('time-input', 'Uhrzeit', 'required');
        $this->form_validation->set_rules('ex-type-select', 'Übungsmaterial', 'required');
        $this->form_validation->set_rules('ex-select', 'Übung', 'required');
        $this->form_validation->set_rules('notes', 'Notizen', '');
        $this->form_validation->set_rules('score', 'Punkte', 'required|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('max-score', 'Maximale Punkte', 'required|greater_than[0]|greater_than_equal_to['.$this->input->post('score').']');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->model('users_model');
            $this->load->model('ex_model');
            $this->load->model('ex_types_model');
            $this->load->model('comp_levels_model');
            $this->load->model('modes_model');

            //data for component client-profile.php
            $data += $this->get_client_profile_data($client_id);
            //data for component lesson-creater.php
            $data['lesson_creater_error'] = TRUE;
            $data += $this->get_lesson_creater_data($client_id);
            //data for component lesson-table.php
            $data += $this->get_lesson_table_data($client_id, 'date', 'desc', 0);

            $this->load->view('clients/view', $data);
        }
        else
        {
            $this->lessons_model->create_lesson($client_id);
            $modified_date = date('Y-m-d', strtotime($this->input->post('date-input')));
            $this->clients_model->set_modified($client_id, $modified_date);
            redirect(base_url() . 'index.php/clients/view/' . $client_id);
        }
    }

    public function edit($id)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'lessons';
        $this->load->library('form_validation');

        $data['lesson'] = $this->lessons_model->get_lesson_by_id($id);

        $this->form_validation->set_rules('date', 'Datum', 'required');
        $this->form_validation->set_rules('time', 'Uhrzeit', 'required');


        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('lessons/edit', $data);
        }
        else
        {
            $this->lessons_model->edit_lesson($id);
            redirect( base_url() . 'index.php/clients/view/'.$data['lesson']['client_id']);
        }
    }

    public function archive()
    {
        $this->check_session();
        $id = $this->uri->segment(3);

        $lesson = $this->lessons_model->get_lesson_by_id($id);
        $this->lessons_model->archive_lesson($id);

        redirect( base_url() . 'index.php/clients/view/' . $lesson['c_id']);
    }

    public function delete()
    {
        $this->check_session();
        $id = $this->uri->segment(3);

        $lesson = $this->lessons_model->get_lesson_by_id($id);
        $this->lessons_model->delete_lesson($id);

        redirect( base_url() . 'index.php/clients/view/' . $lesson['c_id']);
    }
}