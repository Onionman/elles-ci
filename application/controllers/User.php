<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Controller Class for signed-in user
 */
class User extends MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('users_model');
    }

    function index()
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'dashboard';
        $this->load->model('clients_model');
        $this->load->model('user_has_clients_model');
        $this->load->model('lessons_model');

        //data for component lesson-dashboard.php
        $data['lessons_today'] = $this->lessons_model->get_lessons_from_day(date("Y-m-d"));


        $data['search_string'] = '';
        $search_string = $this->input->post('search');

        if(!empty($search_string))
        {
            $this->uri->segment(6, $this->uri->segment(5, 1));
            $data['search_string'] = $this->uri->segment(5, $search_string);
        }
        elseif ($this->uri->segment(5) != null && !empty($this->uri->segment(5)) && $this->uri->segment(6) != null)
        {
            $data['search_string'] = $this->uri->segment(5);

        }

        //data for component clients-table.php
        $data['num_lessons_per_client'] = $this->lessons_model->count_lessons_per_client();

        $clients = $this->clients_model->get_all_clients_of_user('lastname', 'asc', 5, 0, $search_string);
        $data['clients'] = $clients['rows'];

        $data['last_lesson_per_client'] = $this->lessons_model->get_last_lessons_per_client();


        $this->load->view('user/index', $data);
    }

    function logout()
    {
        $newdata = array(
            'user_email' => $_SESSION['email'],
            'logged_in' => FALSE,
        );

        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();

        redirect(base_url() . 'index.php/login/index');
    }

    public function edit()
    {
        $this->check_session();
        $data = $this->globalData;
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $data['nav_active'] = 'user';
        $curr_user = $this->users_model->get_curr_user();
        $data['curr_user'] = $curr_user;

        $this->form_validation->set_rules('firstname', 'Vorname', 'alpha_numeric_spaces');
        $this->form_validation->set_rules('lastname', 'Nachname', 'alpha_numeric_spaces');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('user/edit', $data);
        }
        else
        {
            $this->users_model->set_user($curr_user['id']);
            redirect( base_url() . 'index.php/user/edit/');
        }
    }
}