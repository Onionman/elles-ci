<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 23.05.17
 * Time: 19:22
 *
 * Controller Class for guests who are not signed in
 */
class Login extends CI_Controller
{
    function index()
    {
        $data['nav_active'] = 'home';
        $this->load->view('guest/home', $data);
    }

    function info()
    {
        $data['nav_active'] = 'info';
        $this->load->view('guest/info', $data);
    }
    function login_user(){
        $data['nav_active'] = 'login';
        $this->load->view('guest/login', $data);
    }

    function validate_credentials()
    {
        $this->load->model('users_model');
        $data['nav_active'] = 'login';
        $query = $this->users_model->validate();

        if($query) //if users credentials validated
        {
            $newData = array(
                'email' => $this->input->post('email'),
                'is_logged_in' => TRUE
            );
            $this->session->set_userdata($newData);
            redirect('clients/index');
        }
        else //incorrect username or password
        {
            $data['validate_fail'] = 'Ungültige Email oder Passwort. Bitte versuchen Sie es noch einmal.';
            $this->load->view('guest/login', $data);
        }
    }

    function register()
    {
        $data['nav_active'] = 'register';;
        $this->load->library('Form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_if_email_exists');
        $this->form_validation->set_rules('password', 'Passwort', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('password_confirm', 'Passwort bestätigen', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('guest/register', $data);
        } else {
            $this->load->model('users_model');

            if ($query = $this->users_model->create_user())
            {
                $data['account_created'] = 'Registrierung erfolgreich, Sie können sich nun einloggen.';
                $this->load->view('guest/login', $data);
            }
            else
            {
                $data['account_not_created'] = 'Registrierung nicht erfolgreich.';
                $this->load->view('guest/register', $data);
            }
        }

    }

    //custom callback function
    function check_if_email_exists($requested_email){
        $this->load->model('users_model');

        $mail_available = $this->users_model->check_if_email_exists($requested_email);

        return ($mail_available) ? true : false;
    }
}