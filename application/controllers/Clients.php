<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 29.05.17
 * Time: 11:25
 */
class Clients extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('user_has_clients_model');
        $this->load->model('lessons_model');
    }

    public function index($order_by = 'modified', $sort_by = 'desc', $offset = 0)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'clients';

        //data for component lesson-dashboard.php
        $data += $this->get_lesson_dashboard_data();
        //data for search button
        $search_string = $this->input->post('search');
        $data += $this->get_search_button_data($search_string);
        //data for component clients-table.php
        $data += $this->get_clients_table_data($order_by, $sort_by, $offset, $search_string);


        $this->load->view('clients/index', $data);
    }

    public function view($id, $order_by = 'date', $sort_by = 'desc', $offset = 0)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'clients';
        $this->load->model('users_model');
        $this->load->model('ex_model');
        $this->load->model('ex_types_model');
        $this->load->model('comp_levels_model');
        $this->load->model('modes_model');

        //data for component client-profile.php
        $data += $this->get_client_profile_data($id);
        //data for component lesson-creater.php
        $data += $this->get_lesson_creater_data($id);
        //data for component lesson-table.php
        $data += $this->get_lesson_table_data($id, $order_by, $sort_by, $offset);

        $this->load->view('clients/view', $data);
    }

    public function notes($id)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'clients';
        $data += $this->get_client_profile_data($id);


        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('notes', 'Notizen', 'max_length[1000]');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('clients/notes', $data);
        }
        else
        {
            $this->clients_model->set_client_notes($id);
            redirect( base_url() . 'index.php/clients/notes/'.$id);
        }
    }

    public function stats($id)
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'clients';
        $data += $this->get_client_profile_data($id);
        $this->load->model('charts_model');

        $competences_of_client = $this->lessons_model->get_client_competences($id);

        $index = 0;
        foreach($competences_of_client as $obj):
           $data['chart_data_per_level'][$index] = $this->charts_model->get_client_lessons_chart_data($id, $obj['comp_level_id']);
            $index++;
        endforeach;

        $this->load->view('clients/stats', $data);
    }

    public function create()
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'clients';
        $this->load->library('form_validation');

        //data for component lesson-dashboard.php
        $data += $this->get_lesson_dashboard_data();
        //data for search button
        $search_string = $this->input->post('search');
        $data += $this->get_search_button_data($search_string);
        //data for component clients-table.php
        $data += $this->get_clients_table_data('created', 'asc', 0, '');

        $this->form_validation->set_rules('lastname', 'Nachname', 'trim|required');
        $this->form_validation->set_rules('firstname', 'Vorname', 'trim|required');
        $this->form_validation->set_rules('day', 'Geburtsdatum', 'numeric|required');
        $this->form_validation->set_rules('month', 'Geburtsdatum', 'numeric|required');
        $this->form_validation->set_rules('year', 'Geburtsdatum', 'numeric|required');

        if ($this->form_validation->run() === FALSE)
        {
            $data['client_creater_error'] = TRUE;

            $newData = array(
                'message' => 'Klient wurde noch nicht erstellt! Bitte überprüfen Sie die Angaben!'
            );
            $this->session->set_userdata($newData);

            $this->load->view('clients/index', $data);
        }
        else
        {
            $new_client_id = $this->clients_model->set_client();
            $this->user_has_clients_model->create_relation($new_client_id);
            $new_client = $this->clients_model->get_client_by_id($new_client_id);

            $newData = array(
                'message' => 'Klient wurde erfolgreich erstellt! Das Klienten-Kürzel lautet: '.$new_client['acronym']
            );
            $this->session->set_userdata($newData);

            redirect( base_url() . 'index.php/clients/view/'.$new_client_id);
        }
    }

    public function edit()
    {
        $this->check_session();
        $data = $this->globalData;
        $data['nav_active'] = 'clients';
        $this->load->library('form_validation');

        $id = $this->uri->segment(3);

        $curr_user = $this->users_model->get_curr_user();

        if($id == 1 && $curr_user['id'] != 1) //Max Mustermann is not editable, except for the admin
        {
            redirect( base_url() . 'index.php/clients/notes/'.$id);
        }

        $data['client'] = $this->clients_model->get_client_by_id($id);

        $this->form_validation->set_rules('lastname', 'Nachname', 'trim|required');
        $this->form_validation->set_rules('firstname', 'Vorname', 'trim|required');
        $this->form_validation->set_rules('day', 'Tag', 'numeric|required');
        $this->form_validation->set_rules('month', 'Monat', 'numeric|required');
        $this->form_validation->set_rules('year', 'Jahr', 'numeric|required');
        $this->form_validation->set_rules('created_day', 'Tag', 'numeric|required');
        $this->form_validation->set_rules('created_month', 'Monat', 'numeric|required');
        $this->form_validation->set_rules('created_year', 'Jahr', 'numeric|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('clients/edit', $data);
        }
        else
        {
            $this->clients_model->set_client($id);
            $this->user_has_clients_model->set_active($data['client']['uhc_id']);
            redirect( base_url() . 'index.php/clients/view/'.$id);
        }
    }

    public function delete()
    {
        $this->check_session();
        $client_id = $this->uri->segment(3);
        $this->load->model('users_model');
        $curr_user = $this->users_model->get_curr_user();


        $relation = $this->user_has_clients_model->get_relation($curr_user['id'], $client_id);
        $this->lessons_model->delete_all_lessons_from_relation($relation['id']);
        $this->user_has_clients_model->delete_relation($curr_user['id'], $client_id);
        $this->clients_model->delete_client($client_id);
        redirect( base_url() . 'index.php/clients');
    }
}