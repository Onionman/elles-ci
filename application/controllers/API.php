<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 29.06.17
 * Time: 15:07
 *
 * Elles API provides following HTTP requests:
 *
 * GET lesson
 * POST lesson
 *
 * The address for a HTTP request is:
 * https://www.phonetik.uni-muenchen.de/apps/elles-ci/index.php/API/lesson
 */
class API extends REST_Controller
{
    function __construct()
    {
        parent::__construct();

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['lesson_post']['limit'] = 100; // 100 requests per hour per user/key

        $this->load->model('API_model');
    }

    /**
     * This API function returns a JSON with all lessons of one origin source from the given client.
     *
     * example:
     * user_email = user@user.com
     * client = muma (acronym of a client)
     * origin_source = learning app XY
     */
    public function lesson_get()
    {
        //parameters
        $user_email = $this->get('user_email', TRUE);
        $client_acronym = $this->get('client', TRUE);
        $origin_source = $this->get('origin_source', TRUE);

        $this->load->model('users_model');
        $this->load->model('user_has_clients_model');

        $check_user = $this->users_model->check_if_email_exists($user_email);
        if($check_user == TRUE)
        {
            $this->response(array('status' => 'failed', 'message' => 'email does not exists'));
        } else {
            $result['user_id'] = $this->users_model->get_user_by_email($user_email)['id'];
        }

        $check_client = $this->user_has_clients_model->check_if_relation_exists($result['user_id'], $client_acronym);
        if($check_client == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'no client for '.$user_email));
        } else {
            $result['user_has_clients_id'] = $check_client['id'];
        }

        $lessons = $this->API_model->get_lessons_for_api_get($result['user_id'], $check_client['id'], $origin_source);

        $message = array(
            'user_email' => $user_email,
            'client' => $client_acronym,
            'origin_source' => $origin_source,
            'lessons' => $lessons
        );
        $this->response(array('status' => 'test', 'message' => $message));
    }

    /**
     * This API function creates a new entry in the 'lessons' table.
     *
     * The POST lesson request must have a message in JSON format.
     * Example:
     *
     * {
     * "client": "muma",   <- client acronym
     * "collection": "LRS",
     * "competence": "Rechtschreibung",
     * "competence_level": "1",
     * "exercise": "Eisbären in der Arktis",
     * "exercise_type": "Testübung",
     * "score": 400,
     * "max_score": 500,
     * "mode": "ohne Betreuung",
     * "origin_source": "learning app XY",
     * "timestamp": "2017-08-10 10:00:00",
     * "user_email": "user@user.com"
     * }
     *
     */
    public function lesson_post()
    {
        //data to validate
        $check_data = array(
            "user_email" => $this->post('user_email'),
            "client_acronym" => $this->post('client'),
            "collection_name" => $this->post('collection'),
            "comp_name" => $this->post('competence'),
            "comp_level" => $this->post('competence_level'),
            "ex_name" => $this->post('exercise'),
            "ex_type_name" => $this->post('exercise_type'),
            "mode_name" => $this->post('mode'),
        );

        $result = $this->check_lesson_post_data($check_data);

        //data to take over
        $result['timestamp'] = $this->post('timestamp');
        $datetime = explode(" ",$result['timestamp']);
        $result['date'] = $datetime[0];
        $result['time'] = $datetime[1];

        $result['origin'] = $this->post('origin_source');
        $result['score'] = $this->post('score');
        $result['max_score'] = $this->post('max_score');

        $new_lesson_id = $this->API_model->create_lesson_from_api_post($result);

        if($new_lesson_id == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'lesson post failed.'),
                REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $message = array(
                'status' => 'success',
                'message' => 'added new lesson',
                'lesson id' => $new_lesson_id
            );

            //update client column 'modified'
            $this->load->model('clients_model');
            $this->load->model('users_model');
            $user = $this->users_model->get_user_by_email($check_data['user_email']);
            $client_id = $this->clients_model->get_id($user['id'], $check_data['client_acronym']);
            $this->clients_model->set_modified($client_id, $result['date']);

            $this->response($message + $result, REST_Controller::HTTP_OK);
        }
    }

    /*
     * This function checks data from the POST lesson request
     *
     * If user_email, client_acronym, comp_name, comp_level or mode_name do not exist,
     * this function responses a message with status: failed.
     * If ex_name or ex_type_name do not exist,
     * this function creates a new entry in the database.
     */
    function check_lesson_post_data($data)
    {
        $result = array();
        $this->load->model('users_model');
        $this->load->model('user_has_clients_model');
        //$this->load->model('collections_model');
        $this->load->model('comp_model');
        $this->load->model('comp_levels_model');
        $this->load->model('ex_model');
        $this->load->model('ex_types_model');
        $this->load->model('modes_model');

        $check_user = $this->users_model->check_if_email_exists($data['user_email']);
        if($check_user == TRUE)
        {
            $this->response(array('status' => 'failed', 'message' => 'email does not exists'));
        } else {
            $result['user_id'] = $this->users_model->get_user_by_email($data['user_email'])['id'];
        }

        $check_client = $this->user_has_clients_model->check_if_relation_exists($result['user_id'], $data['client_acronym']);
        if($check_client == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'no client for '.$data['user_email']));
        } else {
            $result['user_has_clients_id'] = $check_client['id'];
        }

        /*
        $check_collection = $this->collections_model->check_if_collection_exists($data['collection_name']);
        if($check_collection == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'collection does not exist'),
                REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $result['collection_id'] = $check_collection['id'];
        }
        */

        $check_comp = $this->comp_model->check_if_comp_exists($data['comp_name']);
        if($check_comp == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'competence does not exist. Only "Lesen" and "Rechtschreibung" allowed.'));
        } else {
            $result['comp_id'] = $check_comp['id'];
        }

        $check_comp_level = $this->comp_levels_model->check_if_level_exists($result['comp_id'], $data['comp_level']);
        if($check_comp_level == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'competence level does not exist'));
        } else {
            $result['comp_level_id'] = $check_comp_level['id'];
        }

        $check_mode = $this->modes_model->check_if_mode_exists($data['mode_name']);
        if($check_mode == FALSE)
        {
            $this->response(array('status' => 'failed', 'message' => 'Mode does not exist. Only "mit Betreuung" and "ohne Betreuung" allowed.'));
        } else {
            $result['mode_id'] = $check_mode['id'];
        }

        $check_ex = $this->ex_model->check_if_ex_exists($result['user_id'], $data['ex_name']);
        if($check_ex == FALSE)
        {
            $result['ex_id'] = $this->ex_model->create_exercise($result['user_id'], $data['ex_name']);
        } else {
            $result['ex_id'] = $check_ex['id'];
        }

        $check_ex_type = $this->ex_types_model->check_if_type_exists($result['user_id'], $data['ex_type_name']);
        if($check_ex_type == FALSE)
        {
            $result['ex_type_id'] = $this->ex_types_model->create_exercise_type($result['user_id'], $data['ex_type_name']);
        } else {
            $result['ex_type_id'] = $check_ex_type['id'];
        }

        return $result;
    }
}