<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 29.05.17
 * Time: 19:38
 *
 * This class includes functions used multiple times by controllers.
 */
class MY_Controller extends CI_Controller
{
    public $globalData = array();

    function __construct()
    {
        parent::__construct();

        if($this->session->userdata('email') != '')
        {
            $this->globalData['curr_user_email'] = $_SESSION['email'];
        }
    }

    function check_session()
    {
        if(!$this->session->userdata('is_logged_in')):
            $this->session->set_flashdata('error', 'Nicht eingeloggt.');
            redirect( base_url() . 'index.php');
        endif;
    }

    /*
     * data for views/lessons/components/lesson-dashboard.php
     */
    function get_lesson_dashboard_data()
    {
        $result['lessons_today'] = $this->lessons_model->get_lessons_from_day(date("Y-m-d"));

        return $result;
    }

    /*
     * data for views/clients/components/client-profile.php
     */
    function get_client_profile_data($client_id)
    {
        $result['client'] = $this->clients_model->get_client_by_id($client_id);

        if (empty($result['client'])) {
            show_404();
        }
        $last_lesson = $this->lessons_model->get_last_lesson($client_id);
        if(!empty($last_lesson)){
            $result['last_lesson'] = $last_lesson;
        }
        return $result;
    }

    /*
     * data for views/lessons/components/lesson-creater.php
     */
    function get_lesson_creater_data($client_id)
    {
        $result['curr_client'] = $this->clients_model->get_client_by_id($client_id);
        $curr_user = $this->users_model->get_curr_user();
        $result['curr_user'] = $curr_user;

        $result['all_ex'] = $this->ex_model->get_exercises();
        $result['all_ex_types'] = $this->ex_types_model->get_ex_types();
        $result['all_comp_levels'] = $this->comp_levels_model->get_levels();
        $result['all_modes'] = $this->modes_model->get_modes();

        return $result;
    }

    /*
     * Daten für views/lessons/components/lesson-table.php
     */
    function get_lesson_table_data($client_id, $order_by, $sort_by, $offset)
    {
        $this->load->library('pagination');
        $limit = 5;
        if($offset > 1)  //Schnelle Notlösung um einen Pagination Bug zu umgehen
            $offset--;

        $all_lessons = $this->lessons_model->get_lessons($client_id, $order_by, $sort_by, $limit, $offset);
        $result['all_lessons'] = $all_lessons['rows'];

        $page_uri = 6; //default page uri
        $result['num_lessons'] = $all_lessons['num_rows'];
        $result['order_by'] = $order_by;
        $result['sort_by'] = $sort_by;
        $result['offset'] = $offset;

        $config = array();
        $config['base_url'] = site_url("clients/view/".$client_id."/".$order_by."/".$sort_by."/");
        $config['total_rows'] = $result['num_lessons'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = $page_uri;
        $config['use_page_numbers'] = TRUE;
        $config['first_link'] = '&laquo; ';
        $config['last_link'] = ' &raquo;';
        $config['next_link'] = '';
        $config['prev_link'] = '';

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        return $result;
    }

    /*
     * data for views/clients/components/clients-table.php
     */
    function get_clients_table_data($order_by, $sort_by, $offset, $search_string)
    {
        $this->load->library('pagination');
        $limit = 10;
        $page_uri = 5; //default page uri

        if($offset > 1)  //Schnelle Notlösung um einen Pagination Bug zu umgehen
            $offset--;

        if(!empty($search_string))
            $page_uri = 6;

        $clients = $this->clients_model->get_all_clients_of_user($order_by, $sort_by, $limit, $offset, $search_string);
        $result['clients'] = $clients['rows'];
        $result['num_clients'] = $clients['num_rows'];
        $result['order_by'] = $order_by;
        $result['sort_by'] = $sort_by;
        $result['offset'] = $offset;
        $result['title'] = 'Klienten';

        $config = array();
        $config['base_url'] = base_url("index.php/clients/index/".$order_by."/".$sort_by."/".$search_string);
        $config['total_rows'] = $result['num_clients'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = $page_uri;
        $config['use_page_numbers'] = TRUE;
        $config['first_link'] = '&laquo; ';
        $config['last_link'] = ' &raquo;';
        $config['next_link'] = '';
        $config['prev_link'] = '';

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        $result['num_lessons_per_client'] = $this->lessons_model->count_lessons_per_client();

        return $result;
    }
    /*
     * data for search button
     */
    function get_search_button_data($search_string)
    {
        $result['search_string'] = '';


        if(!empty($search_string))
        {
            $this->uri->segment(6, $this->uri->segment(5, 1));
            $result['search_string'] = $this->uri->segment(5, $search_string);
        }
        elseif ($this->uri->segment(5) != null && !empty($this->uri->segment(5)) && $this->uri->segment(6) != null)
        {
            $result['search_string'] = $this->uri->segment(5);

        }

        return $result;
    }
}