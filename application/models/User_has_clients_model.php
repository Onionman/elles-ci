<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 14.07.17
 * Time: 22:25
 */
class User_has_clients_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('users_model');
    }

    function set_active($id)
    {
        $data = array(
            'is_active' => $this->input->post('active')
        );
        $this->db->where('id', $id);

        return $this->db->update('user_has_clients', $data);
    }

    function create_relation($client_id)
    {
        $data = array(
            'user_id' => $this->users_model->get_curr_user()['id'],
            'client_id' => $client_id
        );

        return $this->db->insert('user_has_clients', $data);
    }

    function get_relation($user_id, $client_id)
    {
        $query = $this->db
            ->where('user_id', $user_id)
            ->where('client_id', $client_id)
            ->get('user_has_clients')
            ->row_array();

        if (empty($query)){
            return FALSE; //relation doesn't exist
        } else {
            return $query;
        }
    }

    function check_if_relation_exists($user_id, $client_acronym)
    {
        $query = $this->db
            ->where('user_id', $user_id)
            ->where('LOWER(acronym)', strtolower($client_acronym))
            ->join('clients', 'clients.id = user_has_clients.client_id')
            ->get('user_has_clients')
            ->row_array();

        if (empty($query)){
            return FALSE; //relation doesn't exist
        } else {
            return $query;
        }
    }

    function delete_relation($user_id, $client_id)
    {
        $this->db
            ->where('user_id', $user_id)
            ->where('client_id', $client_id);

        return $this->db->delete('user_has_clients');
    }

    function set_client_inactive($id)
    {
        $curr_user = $this->users_model->get_curr_user();

        $query = $this->db
            ->where('id', $id)
            ->where('user_id', $curr_user['id'])
            ->get('user_has_clients')
            ->row_array();

        if($query):
            $data = array(
                'is_active' => FALSE
            );
            $this->db->where('id', $id);
            return $this->db->update('user_has_clients', $data);
        else:
            return FALSE;
        endif;
    }
}