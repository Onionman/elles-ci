<?php
/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 23.05.17
 * Time: 13:30
 */
class Users_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_curr_user()
    {
        $this->db->where('email', $_SESSION['email']);
        $query = $this->db->get('users');

        return $query->row_array();
    }

    public function get_user_by_email($email)
    {
        $query = $this->db
            ->get_where('users', array('email' => $email));
        return $query->row_array();
    }

    public function set_user($id)
    {
        $data = array(
            'firstname' => ucfirst(strtolower($this->input->post('firstname'))),
            'lastname' => ucfirst(strtolower($this->input->post('lastname'))),
            'modified' => mdate('%Y-%m-%d', time())
        );

        $this->db->where('id', $id);
        return $this->db->update('users', $data);
    }

    function create_user()
    {
        $new_member_insert_data = array(
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'created' => mdate('%Y-%m-%d', time())
        );

        return $this->db->insert('users', $new_member_insert_data);
    }

    function validate()
    {
        $this->db->where('email', $this->input->post('email'));
        $this->db->where('password', md5($this->input->post('password')));
        $query = $this->db->get('users');

        if ($query->num_rows() == 1){
            return true;
        }
    }

    function check_if_email_exists($email)
    {
        $query = $this->db
            ->where('email', $email)
            ->get('users');

        if ($query->num_rows() > 0){
            return FALSE; //email already taken
        } else {
            return TRUE;
        }
    }
}