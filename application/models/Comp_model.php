<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 30.06.17
 * Time: 23:24
 */
class Comp_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_all_competences()
    {
        $query = $this->db
            ->select('comp_id, 
                        competence_levels_gui.id AS cl_id,
                        competence_levels_gui.name AS cl_name,
                        competences_gui.name AS c_name,
                        level, remark')
            ->join('competence_levels_gui', 'competence_levels_gui.comp_id = competences_gui.id')
            ->group_by('comp_id, cl_id, cl_name, c_name, level, remark')
            ->order_by('c_name', 'asc')
            ->order_by('level', 'asc')

            ->get('competences_gui')
            ->result_array();

        return $query;
    }

    public function check_if_comp_exists($name)
    {
        $query = $this->db
            ->get_where('competences_gui', array('name' => $name))
            ->row_array();

        if(!empty($query))
        {
            return $query;
        } else {
            return FALSE;
        }
    }
}