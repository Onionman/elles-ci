<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 14.06.17
 * Time: 01:33
 */
class Comp_levels_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_levels()
    {
        $query = $this->db
            ->order_by('level', 'asc')
            ->get('competence_levels_gui')
            ->result_array();
        return $query;
    }

    public function check_if_level_exists($comp_id, $level)
    {
        $query = $this->db
            ->select('*')
            ->from('competence_levels_gui')
            ->where('comp_id', $comp_id)
            ->where('level', $level)
            ->get()->row_array();

        if(!empty($query))
        {
            return $query;
        } else {
            return FALSE;
        }
    }
}