<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 29.05.17
 * Time: 09:45
 */
class Clients_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
    }

    public function get_client_by_id($id)
    {
        $curr_user = $this->users_model->get_curr_user();

        if($id == 1):
            $query = $this->db
                ->select('*,
                        clients.id AS c_id,
                        user_has_clients.id AS uhc_id')
                ->from('clients')
                ->where('clients.id', $id)
                ->where('user_id', 1)
                ->join('user_has_clients', 'clients.id = user_has_clients.client_id')
                ->get();
        else:
            $query = $this->db
                ->select('*,
                            clients.id AS c_id,
                            user_has_clients.id AS uhc_id')
                ->from('clients')
                ->where('user_id', $curr_user['id'])
                ->where('clients.id', $id)
                ->join('user_has_clients', 'clients.id = user_has_clients.client_id')
                ->get();
        endif;

        return $query->row_array();
    }


    function get_id($user_id, $acronym)
    {
        $query = $this->db
            ->select('*,
                            clients.id AS c_id')
            ->from('clients')
            ->where('user_id', $user_id)
            ->where('LOWER(acronym)', strtolower($acronym))
            ->join('user_has_clients', 'clients.id = user_has_clients.client_id')
            ->get()
            ->row_array();

        if (!empty($query)){
            return $query['c_id']; //client exists
        } else {
            return FALSE;
        }
    }

    function get_all_clients_of_user($order_by, $sort_by, $limit, $offset, $search_string)
    {
        $curr_user = $this->users_model->get_curr_user();

        if(empty($search_string)):
            $query = $this->db
                ->select('*,
                            clients.id AS c_id')
                ->where('user_id', $curr_user['id'])
                ->where('is_active', TRUE)
                ->or_where('user_id', 1)
                ->join('clients', 'clients.id = client_id')
                ->limit($limit, ($offset * $limit))
                ->order_by($order_by, $sort_by)
                ->get('user_has_clients')
                ->result_array();
        else:
            $query = $this->db
                ->select('*,
                            clients.id AS c_id')
                ->from('clients')
                ->group_start()
                    ->like('LOWER(firstname)', strtolower($search_string))
                    ->or_like('LOWER(lastname)', strtolower($search_string))
                    ->or_like('LOWER(acronym)', strtolower($search_string))
                    ->group_end()
                ->where('user_id', $curr_user['id'])
                //->where('is_active', TRUE)  inaktive CLIENTS still can be displayed through search button
                ->or_where('user_id', 1)
                ->join('user_has_clients', 'clients.id = user_has_clients.client_id')
                ->limit($limit, ($offset * $limit))
                ->order_by($order_by, $sort_by)
                ->get()
                ->result_array();
        endif;

        $result['rows'] = $query;

        $count_clients_query = $this->db
            ->select('COUNT(*) as count', FALSE)
            ->where('is_active', TRUE)
            ->where('user_id', $curr_user['id'])
            ->get('user_has_clients')
            ->result();

        $result['num_rows'] = $count_clients_query[0]->count;

        return $result;
    }

    public function set_client($id = 0)
    {
        $this->load->helper('text');

        //converts user input in: Y-m-d
        $d_input = $this->input->post('day');
        $m_input = $this->input->post('month');
        $Y_input = $this->input->post('year');
        $timestamp = strtotime($d_input.'.'.$m_input.'.'.$Y_input);
        $d_created = $this->input->post('created_day');
        $m_created = $this->input->post('created_month');
        $Y_created = $this->input->post('created_year');
        $created = strtotime($d_created.'.'.$m_created.'.'.$Y_created);
        if ($id == 0)
            $created = time();

        $lastname_temp = $this->specialchar($this->input->post('lastname'));
        $firstname_temp = $this->specialchar($this->input->post('firstname'));

        $data = array(

            'lastname' => ucfirst(strtolower($this->input->post('lastname'))),
            'firstname' => ucwords(strtolower($this->input->post('firstname'))),
            'acronym' => strtolower(substr($lastname_temp, 0,2)).
                strtolower(substr($firstname_temp, 0,2)),
            'gender' => $this->input->post('gender'),
            'birthday' => date('Y-m-d', $timestamp),
            'street' => ucfirst($this->input->post('street')),
            'zip_code' => $this->input->post('zip_code'),
            'location' => ucfirst($this->input->post('location')),
            'phone' => $this->input->post('phone'),
            'phone_mobile' => $this->input->post('phone_mobile'),
            'email' => $this->input->post('email'),
            'created' => date('Y-m-d', $created),
            'modified' => date('Y-m-d', $created)
        );

        if ($id == 0) {
            $this->db->insert('clients', $data);
            return $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            return $this->db->update('clients', $data);
        }
    }

    public function set_client_notes($id)
    {
        $data = array(
            'notes' => $this->input->post('notes')
        );
        $this->db->where('id', $id);

        return $this->db->update('clients', $data);
    }


    public function set_modified($id, $date)
    {
        $data = array(
            'modified' => $date
        );
        $this->db->where('id', $id);

        return $this->db->update('clients', $data);
    }


    public function delete_client($id)
    {
        $this->db
            ->select('*')
            ->from('clients')
            ->where('clients.id', $id);

        return $this->db->delete('clients');
    }

    public function check_if_client_exists($client_acronym)
    {
        $query = $this->db
            ->from('clients')
            ->where('LOWER(acronym)', strtolower($client_acronym))
            ->get()->row_array();

        if (!empty($query)){
            return $query; //client exists
        } else {
            return FALSE;
        }
    }

    function specialchar($string)
    {
        $string = str_replace("ä", "ae", $string);
        $string = str_replace("ü", "ue", $string);
        $string = str_replace("ö", "oe", $string);
        $string = str_replace("Ä", "Ae", $string);
        $string = str_replace("Ü", "Ue", $string);
        $string = str_replace("Ö", "Oe", $string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("´", "", $string);

        return $string;
    }
}