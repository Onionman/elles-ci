<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 29.05.17
 * Time: 15:37
 */
class Ex_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('users_model');
    }

    public function get_exercises()
    {
        $curr_user = $this->users_model->get_curr_user();
        $query = $this->db
            ->where('user_id', $curr_user['id'])
            ->get('exercises')
            ->result_array();
        return $query;
    }

    public function get_ex_by_id($id)
    {
        $curr_user = $this->users_model->get_curr_user();

        $query = $this->db
            ->where('user_id', $curr_user['id'])
            ->get_where('exercises', array('id' => $id))
            ->row_array();

        return $query;
    }

    public function create_exercise($user_id, $name)
    {
        $data = array(
            'user_id' => $user_id,
            'name' => $name
        );
        $this->db->insert('exercises', $data);

        return $this->db->insert_id();
    }

    public function check_if_ex_exists($user_id, $name)
    {
        $query = $this->db
            ->where('user_id', $user_id)
            ->get_where('exercises', array('name' => $name))
            ->row_array();

        if(!empty($query))
        {
            return $query;
        } else {
            return FALSE;
        }
    }
}