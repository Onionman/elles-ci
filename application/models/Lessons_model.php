<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 10.07.17
 * Time: 15:12
 */
class Lessons_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('clients_model');
        $this->load->model('user_has_clients_model');
    }

    function create_lesson($client_id)
    {
        //check if client exists for current user
        $user_id = $this->users_model->get_curr_user()['id'];
        if(!$this->user_has_clients_model->get_relation($user_id, $client_id)):
            return FALSE;
        else:
            $user_has_client = $this->user_has_clients_model->get_relation($user_id, $client_id);
        endif;

        //checks which radio button is selected
        if($this->input->post('radios') == 'read')
        {
            $comp_level_id = $this->input->post('level-select-read');
        }
        else
        {
            $comp_level_id = $this->input->post('level-select-write');
        }

        //converts user input in: Y-m-d G:m:s
        $date_input = $this->input->post('date-input');
        $time_input = $this->input->post('time-input');
        $curr_seconds = date('s',time());
        $timestamp = strtotime($date_input.' '.$time_input.':'.$curr_seconds);

        //custom user input 'exercise' or 'exercise_type'
        $ex_id = $ex_id = $this->input->post('ex-select');
        if($this->input->post('ex-select') == 'custom')
        {
            $this->load->model('ex_model');
            $ex_name = $this->input->post('new_ex');
            $ex_id = $this->ex_model->create_exercise($user_id, $ex_name);
        }

        $ex_type_id = $ex_type_id = $this->input->post('ex-type-select');
        if($this->input->post('ex-type-select') == 'custom')
        {
            $this->load->model('ex_types_model');
            $ex_type_name = $this->input->post('new_ex_type');
            $ex_type_id = $this->ex_types_model->create_exercise_type($user_id, $ex_type_name);
        }

        $data = array(
            'user_has_clients_id' => $user_has_client['id'],
            'mode_id' =>  $this->input->post('mode-select'),
            'comp_level_id' =>  $comp_level_id,
            'ex_id' =>  $ex_id,
            'ex_type_id' =>  $ex_type_id,
            'time' => date('G:i:s',$timestamp),
            'date' => date('Y-m-d', $timestamp),
            'score' =>  $this->input->post('score'),
            'max_score' =>  $this->input->post('max-score'),
            'notes' =>  $this->input->post('notes'),
            'origin' => '' //when a lesson was exercised without an app, then the 'origin' column is empty
        );
        return $this->db->insert('lessons', $data);
    }

    /*
     * @param: $client_id ID from the table 'Clients'
     * @param: $order_by, $sort_by attribute from the table 'Lessons'
     * @param: $limit, $offset parameters for the codeigniter pagination library
     * @return: num_rows: number of all lessons of a client; rows: all lessons of a client in an array
     */
    function get_lessons($client_id, $order_by = 'date', $sort_by = 'desc', $limit = 10, $offset = 0){

        $curr_user = $this->users_model->get_curr_user();

        $query = $this->db
            ->select('*, 
                            exercises.name AS e_name,
                            exercise_types.name AS et_name,
                            lessons.id AS l_id,
                            competence_levels_gui.name AS cl_name,
                            competence_levels_gui.comp_id AS cl_comp_id,
                            competence_levels_gui.level AS cl_level,
                            competence_levels_gui.remark AS cl_remark,
                            users.lastname AS u_lastname,
                            modes_gui.name AS m_name')
            ->from('lessons')
            ->where('user_has_clients.client_id', $client_id)
            ->where('archived', FALSE)
            ->group_start()
                ->where('user_has_clients.user_id', $curr_user['id'])
                ->or_where('user_has_clients.user_id', 1)   //Admin ID
            ->group_end()
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('users', 'users.id = user_has_clients.user_id')
            ->join('modes_gui', 'modes_gui.id = mode_id')
            ->join('competence_levels_gui', 'competence_levels_gui.id = comp_level_id')
            ->join('exercises', 'exercises.id = ex_id')
            ->join('exercise_types', 'exercise_types.id = ex_type_id')
            ->limit($limit, $offset * $limit)
            ->order_by($order_by, $sort_by)
            ->order_by('time', $sort_by)
            ->get()
            ->result_array();

        $result['rows'] = $query;
        $result['num_rows'] = $this->count_all_lessons($client_id);

        return $result;
    }

    function count_all_lessons($client_id)
    {
        $countQuery = $this->db
            ->select('COUNT(*) as count', FALSE)
            ->where('user_has_clients.client_id', $client_id)
            ->where('archived', FALSE)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->get('lessons')
            ->result();

        return $countQuery[0]->count;
    }

    function count_lessons_per_client()
    {
        $query = $this->db
            ->select('COUNT(lessons.id) AS num_lessons,
                        client_id')
            ->where('archived', FALSE)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->group_by('client_id')
            ->get('lessons')
            ->result_array();

        return $query;
    }

    function get_last_lesson($client_id){

        $query = $this->db
            ->select('*,
                            exercise_types.id AS et_id,
                            exercise_types.name AS et_name,
                            exercises.id AS ex_id,
                            exercises.name AS ex_name,
                            competence_levels_gui.id AS cl_id,
                            competence_levels_gui.comp_id AS cl_comp_id,
                            competence_levels_gui.name AS cl_name,
                            competence_levels_gui.remark AS cl_remark')
            ->from('lessons')
            ->where('archived', FALSE)
            ->where('client_id', $client_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('competence_levels_gui', 'comp_level_id = competence_levels_gui.id')
            ->join('exercises', 'ex_id = exercises.id')
            ->join('exercise_types', 'ex_type_id = exercise_types.id')
            ->order_by('date' , 'desc')
            ->get()
            ->first_row('array');

        return $query;
    }

    function get_last_lessons_per_client()
    {

        $curr_user = $this->users_model->get_curr_user();
        $query = $this->db
            ->select('lessons.id, client_id, date, time')
            ->from('lessons')
            ->where('archived', FALSE)
            ->where('user_id', $curr_user['id'])
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->group_by('lessons.id, client_id')
            ->order_by('date', 'desc')
            ->get()
            ->result_array();

        return $query;

    }

    function get_lessons_from_day($day)
    {
        $curr_user = $this->users_model->get_curr_user();

        $query = $this->db
            ->select('*')
            ->from('lessons')
            ->where('date', $day)
            ->where('archived', FALSE)
            ->where('user_id', $curr_user['id'])
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('clients', 'clients.id = client_id')
            ->order_by('time', 'asc')
            ->get()
            ->result_array();

        $result['rows'] = $query;

        $countQuery = $this->db
            ->select('COUNT(*) as count', FALSE)
            ->where('user_id', $curr_user['id'])
            ->where('date', $day)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->get('lessons')
            ->result();

        $result['num_rows'] = $countQuery[0]->count;

        return $result;
    }

    function get_lesson_by_id($lesson_id)
    {
        $query = $this->db
            ->select('*, 
                            modes_gui.name AS m_name,
                            exercises.name AS e_name,
                            exercise_types.name AS et_name,
                            lessons.id AS l_id,   
                            competences_gui.name AS c_name,
                            competence_levels_gui.name AS cl_name,
                            competence_levels_gui.remark AS cl_remark,
                            users.id AS u_id,
                            users.email AS u_email,
                            users.lastname AS u_lastname,
                            clients.id AS c_id,
                            clients.firstname AS cl_firstname,
                            clients.lastname AS cl_lastname')
            ->from('lessons')
            ->where('lessons.id', $lesson_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('users', 'users.id = user_id')
            ->join('clients', 'clients.id = client_id')
            ->join('modes_gui', 'modes_gui.id = mode_id')
            ->join('competence_levels_gui', 'competence_levels_gui.id = comp_level_id')
            ->join('competences_gui', 'competence_levels_gui.comp_id = competences_gui.id')
            ->join('exercises', 'exercises.id = ex_id')
            ->join('exercise_types', 'exercise_types.id = ex_type_id')
            ->get();

        return $query->row_array();
    }

    function get_client_competences($client_id)
    {
        $query = $this->db
            ->distinct()
            ->select('comp_level_id')
            ->where('archived', FALSE)
            ->where('client_id', $client_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->get('lessons')
            ->result_array();
        $result = $query;
        return $result;
    }

    function get_all_exercises()
    {
        $user_id = $this->users_model->get_curr_user()['id'];

        $query = $this->db
            ->distinct()
            ->select('ex_id AS e_id, name, count(*) as count')
            ->where('archived', FALSE)
            ->where('user_id', $user_id)
            ->join('exercises', 'exercises.id = ex_id')
            ->group_by('ex_id, name')
            ->order_by('count', 'desc')
            ->get('lessons')
            ->result_array();

        return $query;
    }
    function get_all_exercise_types()
    {
        $user_id = $this->users_model->get_curr_user()['id'];

        $query = $this->db
            ->select('exercise_types.id AS et_id, name, count(*) as count')
            ->where('archived', FALSE)
            ->where('user_id', $user_id)
            ->join('exercise_types', 'exercise_types.id = ex_type_id')
            ->group_by('exercise_types.id, name')
            ->order_by('count', 'desc')
            ->get('lessons')
            ->result_array();

        return $query;
    }
    function get_all_competences_count()
    {
        $user_id = $this->users_model->get_curr_user()['id'];

        $query = $this->db
            ->select('comp_level_id AS cl_id, user_id, count(*) as count')
            ->where('archived', FALSE)
            ->where('user_id', $user_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->group_by('cl_id, user_id')
            ->get('lessons')
            ->result_array();

        return $query;
    }

    function edit_lesson($id = 0)
    {
        $data = array(
            'timestamp' => $this->input->post('timestamp'),
        );

        $this->db->from('lessons')->where('id', $id);
        return $this->db->update('lessons', $data);
    }

    function delete_all_lessons_from_relation($relation_id)
    {
        $this->db
            ->where('user_has_clients_id', $relation_id)
            ->delete('lessons');
    }

    function archive_lesson($id)
    {
        $data = array(
            'archived' => TRUE,
        );

        $this->db->from('lessons')->where('id', $id);
        return $this->db->update('lessons', $data);
    }

    public function delete_lesson($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('lessons');
    }
}