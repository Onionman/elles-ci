<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 12.07.17
 * Time: 15:03
 */
class Charts_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('clients_model');
        $this->load->model('user_has_clients_model');
        $this->load->model('lessons_model');
    }

    function get_all_client_lessons_chart_data($client_id)
    {
        $query = $this->db
            ->select('
                lessons.date AS l_date, 
                firstname, 
                lastname,
                competences_gui.name AS c_name,
                competence_levels_gui.level AS cl_level,
                competence_levels_gui.name AS cl_name,
                score, 
                max_score
                ')
            ->where('archived', FALSE)
            ->where('client_id', $client_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('clients', 'clients.id = client_id')
            ->join('competence_levels_gui', 'competence_levels_gui.id = comp_level_id')
            ->join('competences_gui', 'competences_gui.id = competence_levels_gui.comp_id')
            ->join('modes_gui', 'modes_gui.id = mode_id')
            ->join('exercises', 'exercises.id = ex_id')
            ->join('exercise_types', 'exercise_types.id = ex_type_id')
            ->order_by('c_name', 'asc')
            ->order_by('cl_level', 'asc')
            ->order_by('date', 'desc')
            ->get('lessons')
            ->result_array();

        return $query;
    }

    function get_client_lessons_chart_data($client_id, $comp_level_id)
    {
        $query = $this->db
            ->select('
                lessons.date AS l_date, 
                firstname, 
                lastname,
                competences_gui.name AS c_name,
                competence_levels_gui.id AS cl_id,
                competence_levels_gui.level AS cl_level,
                competence_levels_gui.name AS cl_name,
                exercises.name AS ex_name,
                exercise_types.name AS et_name,
                score, 
                max_score
                ')
            ->where('archived', FALSE)
            ->where('client_id', $client_id)
            ->where('comp_level_id', $comp_level_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('clients', 'clients.id = client_id')
            ->join('competence_levels_gui', 'competence_levels_gui.id = comp_level_id')
            ->join('competences_gui', 'competences_gui.id = competence_levels_gui.comp_id')
            ->join('modes_gui', 'modes_gui.id = mode_id')
            ->join('exercises', 'exercises.id = ex_id')
            ->join('exercise_types', 'exercise_types.id = ex_type_id')
            ->order_by('c_name', 'asc')
            ->order_by('cl_level', 'asc')
            ->order_by('date', 'desc')
            ->get('lessons')
            ->result_array();

        return $query;
    }

    function get_modes_chart_data()
    {
        $user_id = $this->users_model->get_curr_user()['id'];
        $query = $this->db
            ->select('modes_gui.name AS m_name, user_id, count(*) as count')
            ->where('archived', FALSE)
            ->where('user_id', $user_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('modes_gui', 'modes_gui.id = mode_id')
            ->group_by('m_name, user_id')
            ->get('lessons')
            ->result_array();

        return $query;
    }

    function get_apps_chart_data()
    {
        $user_id = $this->users_model->get_curr_user()['id'];
        $query = $this->db
            ->select('origin, user_id, count(*) as count')
            ->where('archived', FALSE)
            ->where('user_id', $user_id)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->group_by('origin, user_id')
            ->get('lessons')
            ->result_array();

        return $query;
    }
}