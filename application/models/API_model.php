<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 10.07.17
 * Time: 15:20
 */
class API_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_lessons_for_api_get($user_id, $client_id, $origin_source)
    {
        $query = $this->db
            ->select('origin, date, time, score, max_score, 
                        competence_levels_gui.level AS competence_level, 
                        competence_levels_gui.name AS competence_level_name, 
                        competence_levels_gui.remark competencel_level_remark, 
                        competences_gui.name AS competence, 
                        exercises.name AS exercise,
                        exercise_types.name AS exercise_type,
                        modes_gui.name AS mode')
            ->where('user_has_clients.user_id', $user_id)
            ->where('user_has_clients.client_id', $client_id)
            ->where('origin', $origin_source)
            ->join('user_has_clients', 'user_has_clients.id = user_has_clients_id')
            ->join('competence_levels_gui', 'competence_levels_gui.id = comp_level_id')
            ->join('competences_gui', 'competences_gui.id = comp_id')
            ->join('modes_gui', 'modes_gui.id = mode_id')
            ->join('exercises', 'exercises.id = ex_id')
            ->join('exercise_types', 'exercise_types.id = ex_type_id')
            ->get('lessons')
            ->result_array();

        return $query;
    }

    function create_lesson_from_api_post($data)
    {
        $data = array(
            'user_has_clients_id' => $data['user_has_clients_id'],
            'mode_id' =>  $data['mode_id'],
            'comp_level_id' =>  $data['comp_level_id'],
            'ex_id' =>  $data['ex_id'],
            'ex_type_id' => $data['ex_type_id'],
            'date' => $data['date'],
            'time' => $data['time'],
            'score' =>  $data['score'],
            'max_score' =>  $data['max_score'],
            'notes' =>  '',
            'origin' => $data['origin']
        );
        $this->db->insert('lessons', $data);

        return $this->db->insert_id();
    }
}