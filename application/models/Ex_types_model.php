<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 12.06.17
 * Time: 15:57
 */
class Ex_types_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('users_model');
    }

    public function get_ex_types()
    {
        $curr_user = $this->users_model->get_curr_user();

        $query = $this->db
            ->where('user_id', $curr_user['id'])
            ->get('exercise_types')
            ->result_array();
        return $query;
    }

    public function create_exercise_type($user_id, $name)
    {
        $data = array(
            'user_id' => $user_id,
            'name' => $name
        );
        $this->db->insert('exercise_types', $data);

        return $this->db->insert_id();
    }

    public function check_if_type_exists($user_id, $name)
    {
        $query = $this->db
            ->where('user_id', $user_id)
            ->get_where('exercise_types', array('name' => $name))
            ->row_array();

        if(!empty($query))
        {
            return $query;
        } else {
            return FALSE;
        }
    }
}