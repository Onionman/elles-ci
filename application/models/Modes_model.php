<?php

/**
 * Created by PhpStorm.
 * User: Philmac
 * Date: 13.06.17
 * Time: 01:54
 */
class Modes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_modes()
    {
        $query = $this->db
            ->select('*')
            ->from('modes_gui')
            ->get()
            ->result_array();

        return $query;
    }
    public function create_mode($name)
    {
        $data = array(
            'name' => $name
        );
        $this->db->insert('modes_gui', $data);

        return $this->db->insert_id();
    }

    public function check_if_mode_exists($name)
    {
        $query = $this->db
            ->get_where('modes_gui', array('name' => $name))
            ->row_array();

        if(!empty($query))
        {
            return $query;
        } else {
            return FALSE;
        }
    }
}