<table class="info-table" align="center">
    <tr>
       <td style="text-align: left">Name</td>
       <td style="text-align: center; padding: 0">Anzahl</td>
        <td style="text-align: center" colspan='2'>
                Aktion
        </td>

    </tr>
    <?php foreach ($all_exercises as $exercise):

        ?>
        <tr class="table-tr">

            <td>
                <div class="form-label"><?php echo $exercise['name']; ?></div>
            </td>
            <td style="text-align: center;">
                <div class="form-label"><?php echo $exercise['count']; ?></div>
            </td>

            <td style="text-align: center; padding: 0;">
                <a href="#" class="table-action-a">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
            <td style="text-align: center; padding: 0;">
                <a href="#" class="table-action-a">
                    <i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
