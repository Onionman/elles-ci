<table class="info-table" align="center">
    <tr>
        <td style="text-align: left">Name</td>
        <td style="text-align: center; padding: 0; margin: 0">Anzahl</td>
        <td style="text-align: center" colspan='2'>
            Aktion
        </td>
    </tr>
    <?php foreach ($all_exercise_types as $exercise_type):

        ?>
        <tr class="table-tr">
            <td>
                <div class="form-label"><?php echo $exercise_type['name']; ?></div>
            </td>
            <td style="text-align: center; padding: 0">
                <div class="form-label"><?php echo $exercise_type['count']; ?></div>
            </td>
            <td style="text-align: center; padding: 0;">
                <a href="#" class="table-action-a">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
            <td style="text-align: center; padding: 0;">
                <a href="#" class="table-action-a">
                    <i class="fa fa-trash"></i></a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
