<?php $this->load->view('default/header'); ?>

<h2>Account Einstellungen</h2>

<?php echo form_open('user/edit/'); ?>

<table class="layout-table" align="center">
    <col width="25%" />
    <col width="50%" />
    <col width="25%" />
    <tr>
        <td>
        </td>
        <td>
            <table class="form-table" align="center">
                <col width="28%"/>
                <col width="72%"/>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Vorname:', '', array('class' => 'form-label')); ?>
                    </td>
                    <td class="form-table-input-td">
                    <?php echo form_input('firstname', $curr_user['firstname'], 'placeholder="Vorname" id="form-table-input"'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Nachname:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('lastname', $curr_user['lastname'], 'placeholder="Nachname" id="form-table-input"');?>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td class="form-table-label-td">
                        <?php echo form_label('Zeitzone', '', array('class' => 'form-label')); ?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo timezone_menu('UTC','','', 'class="form-label"'); ?>
                    </td>
                </tr>
                <tr style="display: none">
                    <td></td>
                    <td>
                        <a href="<?php echo site_url('user/delete/'.$curr_user['id']); ?>"
                           onClick="return confirm('Sicher?')"
                           class="table-action-a"> Account löschen?</a>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">

                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_submit('submit', 'Speichern'); ?>
                    </td>
                </tr>
            </table>

        </td>
        <td class="layout-table-td">
        </td>
    </tr>
</table>

<?php
echo validation_errors();
echo form_close(); ?>

<?php $this->load->view('default/footer'); ?>

