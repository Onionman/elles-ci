<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Elles Therapie Tool</title>
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>webroot/css/elles-theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>webroot/css/elles-theme-header-navigation.css">
    <link rel="stylesheet" href="<?php echo base_url();?>webroot/css/elles-theme-forms.css">
    <link rel="stylesheet" href="<?php echo base_url();?>webroot/css/elles-theme-table.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>webroot/css/font-awesome.css">

</head>
<header>
    <div>
        <ul id="header-navigation-ul">
                <li class="header-navigation-li" id="brand-name">Elles</li>
                <li class="header-navigation-li"><a href="<?php echo site_url('clients/index'); ?>"
                        <?php if($nav_active=='dashboard'){echo 'id = "active"';}?>>Dashboard</a></li>
                <li class="header-navigation-li"><a href="<?php echo site_url('lessons/index'); ?>"
                        <?php if($nav_active=='lessons'){echo 'id = "active"';}?>>Übungen</a></li>
            <li class="header-navigation-li" style="display: none"><a href="<?php echo site_url('clients/index'); ?>"
                    <?php if($nav_active=='clients'){echo 'id = "active"';}?>>Klienten</a></li>
        </ul>
    </div>
    <div id="login-logout-div">
        <p class="unlinked-text align-right">Angemeldet als <?php echo $curr_user_email; ?>
            &nbsp;<a href="<?php echo site_url('user/logout'); ?>" id="login-logout">Abmelden</a>
        </p>
        <a href="<?php echo site_url('user/edit'); ?>"
            <?php if($nav_active=='user'){echo 'id = "active"';}?> id="login-logout">
            <i class="fa fa-cog"></i> Einstellungen</a>
    </div>
</header>
<body>
<div id="blur-background">
    <!-- Blur Effekt bei einer Dialog Box-->
</div>

