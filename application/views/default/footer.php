<footer style="color: white;">
    <?php echo str_repeat("<br >",5);?>
    <p class="hidden-text">Page rendered in <strong>{elapsed_time}</strong> seconds.
        <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
    <p class="hidden-text">Session: <?php print_r($this->session->all_userdata());?></p>
</footer>

<script src="<?php echo base_url('webroot/jquery/jquery.min.js');?>"></script>
<script src="<?php echo base_url('bower_components/chart.js/dist/Chart.bundle.js');?>"></script>
<script src="<?php echo base_url('webroot/js/elles-java.js');?>"></script>
</body>
</html>

