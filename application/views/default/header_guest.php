<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Elles Therapie Tool</title>
    <link href="https://fonts.googleapis.com/css?family=Khula" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>webroot/css/elles-theme.css">
    <link rel="stylesheet" href="<?php echo base_url()?>webroot/css/elles-theme-header-navigation.css">
    <link rel="stylesheet" href="<?php echo base_url()?>webroot/css/elles-theme-forms.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>webroot/css/font-awesome.css">
</head>
<header>
    <div>
        <ul id="header-navigation-ul">
            <li class="header-navigation-li" id="brand-name">Elles</li>
            <li class="header-navigation-li"><a href="<?php echo site_url('login/index'); ?>"
                    <?php if($nav_active =='home'){echo 'id = "active"';}?>>Startseite</a></li>
            <li class="header-navigation-li"><a href="<?php echo site_url('login/register'); ?>"
                    <?php if($nav_active =='register'){echo 'id = "active"';}?>>Registrieren</a></li>
        </ul>
    </div>
    <div id="login-logout-div">
        <p class="unlinked-text align-right">Sie sind nicht angemeldet.
            <a href="<?php echo site_url('login/login_user'); ?>" id="login-logout">Anmelden</a>
        </p>
    </div>
</header>
<body>

