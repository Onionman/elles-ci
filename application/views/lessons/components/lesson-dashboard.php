<table style="width: 100%; margin: 0 5px 0 5px;">
    <col width="50%" />
    <col width="50%" />
    <?php if(isset($lessons_today)): ?>
        <tr>
            <td style="padding: 15px">
                <p>+ <?php echo $lessons_today['num_rows']?></p>
                <p>Übungen</p>
            </td>

            <?php if($lessons_today['num_rows'] == 0): ?>
                <td>
            <?php else: ?>
                <!--<td style="border-left: solid 1px; padding: 30px;">-->
                <td style="text-align: center;">
            <?php endif; ?>

                <div style="text-align: left; padding: 0 0 0 20%">
                <?php foreach ($lessons_today['rows'] as $lesson): ?>
                    <i class="fa fa-clock-o"></i>
                    <?php echo date_format(new DateTime($lesson['time']), 'G:i'); ?>
                    <?php echo ' '; ?>
                    <a href="<?php echo site_url('clients/view/'.$lesson['client_id']); ?>">
                        <?php echo $lesson['acronym']; ?>
                    </a>
                    <br/>
                <?php endforeach; ?>
                </div>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td><p>Noch keine Übungen</p></td>
            <td></td>
        </tr>
    <?php endif; ?>

    <?php if(isset($lessons_week)): ?>
    <?php else: ?>
    <?php endif; ?>
</table>
