<table class="layout-table" align="center">
    <col width="15%" />
    <col width="82%" />
    <col width="3%" />
    <tr>
        <td></td>
        <td style="text-align: right">
            <?php
            $time = time();

            echo form_label('Datum: ', '', array('class' => 'form-label'));
            echo date_format(new DateTime($lesson['timestamp']), 'd.m.Y');
            echo ' ';
            echo form_label('Uhrzeit: ', '', array('class' => 'form-label'));
            echo date_format(new DateTime($lesson['timestamp']), 'H:i');
            ?>
            <br/>
            <?php
            echo form_label($lesson['m_name'].':','', array('class' => 'form-label'));
            if(isset($lesson['u_lastname'])):
                echo $lesson['u_lastname'];
            else:
                echo $lesson['u_email'];
            endif;
            ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td><br/></td>
    </tr>
    <tr>
        <td class="form-table-label-td">
            <?php echo form_label('Patient:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td">
            <?php echo $lesson['cl_firstname'].' '.$lesson['cl_lastname']?>
        </td>
        <td></td>
    </tr>
    <!--
    <tr>
        <td class="form-table-label-td">
            <?php //echo form_label('Lernziel:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td">
            <?php //echo $lesson['c_name']?>
        </td>
        <td></td>
    </tr>
    -->
    <tr>
        <td class="form-table-label-td" style="vertical-align: top">
            <?php echo form_label('Notizen:', '', array('class' => 'form-label'));?>
        <td class="form-table-input-td form-label">
            <?php
            if(isset($lesson['remark1'])):
                echo '-';
            else:
                echo $lesson['remark2'];
            endif;
            ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="form-table-label-td" style="vertical-align: top">
            <?php echo form_label('Umsetzung:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td form-label">
            <?php
            if(empty($lesson['remark2'])):
                echo '-';
            else:
                echo $lesson['remark2'];
            endif;
            ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="form-table-label-td" style="vertical-align: top">
            <?php echo form_label('Verlauf:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td form-label">
            <?php
            if(isset($lesson['remark3'])):
                echo '-';
            else:
                echo $lesson['remark2'];
            endif;
            ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="form-table-label-td" style="vertical-align: top">
            <?php echo form_label('Übungen:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td">
            <?php $this->load->view('lessons/components/lesson-results'); ?>
        </td>
        <td></td>
    </tr>
</table>



