<div>
    <table class="info-table" align="center" style="width: 100%">
        <col width="70%"/>
        <col width="15%"/>
        <col width="15%"/>
        <?php
        foreach ($lessons_from_day['levels_rows'] as $levels):
        ?>
            <tr class="info-table-header">
            <td>
                <table style="border-collapse: collapse; border-spacing:0;" cellspacing="0">
                    <!--ich krieg den Abstand zwischen den zwei Rows nicht klein! -->
                    <tr style="padding-bottom: 0; border: none; margin: 0">
                        <td style="padding: 0; border: none; margin: 0">
                            <?php if($levels['cl_comp_id'] == 1): ?>
                                <img style="padding: 0" src="<?php echo base_url();?>webroot/img/book_small_new.png" alt="read_img"/>
                            <?php else: ?>
                                <img style="padding: 0" src="<?php echo base_url();?>webroot/img/pen_small_new.png" alt="write_img"/>

                            <?php endif; ?>
                        </td>
                        <td style="padding: 0; margin: 0; border: none">
                            <label class="form-label" style="padding: 0; margin: 0; border: none">
                                <?php echo 'Stufe '. $levels['cl_level'].': '.$levels['cl_name']?>
                            </label>
                        </td>
                    </tr>
                    <tr style="padding-top: 0; border: none; margin: 0">
                        <td style="padding: 0; border: none; margin: 0"></td>
                        <td style="padding: 0; border: none; margin: 0;">

                            <label class="form-label gray" style="padding: 0; margin: 0; border: none">
                                <?php echo $levels['cl_remark'] ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </td>
            <td></td>
            <td></td>
            </tr>
            <?php
            foreach ($lessons_from_day['rows'] as $lesson):

                if($lesson['cl_id'] == $levels['cl_id']):
                ?>
                <tr>
                    <td style="text-align: center">
                        <?php echo $lesson['et_name'];?>
                        <br/>
                        <div class="form-label gray"><?php echo $lesson['e_name'];?></div>
                    </td>
                    <td style="text-align: center">
                        <div class="form-label score"><?php echo $lesson['score'];?></div>
                        <div class="form-label">von <?php echo $lesson['max_score'];?></div>
                    </td>
                    <td style="text-align: center">
                        <?php
                        if($lesson['max_score'] == 0): //Fehlerbehandlung 'division by zero'
                            echo '0';
                        else:
                            echo round($lesson['score']/$lesson['max_score']*100, 0);
                        endif;
                        echo '%';
                        ?>
                    </td>
                </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>

    </table>
</div>