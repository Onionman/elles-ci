
<?php
//echo validation_errors();
echo form_open('lessons/create/'.$curr_client['id']);
?>

<table class="layout-table" align="center">

    <tr>
        <td></td>
        <td style="text-align: right">
            <?php
            $time = time();

            echo form_label('Datum: ', '', array('class' => 'form-label'));
            echo form_input('date-input',mdate('%d.%m.%Y', $time), 'placeholder="TT.MM.JJJJ" id="input-city"');
            echo form_label('Uhrzeit: ', '', array('class' => 'form-label'));
            echo form_input('time-input',mdate('%H:%i', $time), 'placeholder="SS:MM" id="input-zip-code"');
            ?>
            <br/>
            <select name="mode-select" class="form-label">
                <?php if(count($all_modes)):
                    foreach($all_modes as $mode): ?>
                        <option value= <?php echo $mode['id']?>><?php echo $mode['name']?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
            <?php
            if(isset($curr_user['lastname'])):
                echo form_label($curr_user['lastname'],'', array('class' => 'unlinked-text'));
            else:
                echo form_label($curr_user['email'],'', array('class' => 'unlinked-text'));
            endif;
            ?>
        </td>
    </tr>
    <tr>
        <td class="form-table-label-td">
            <?php echo form_label('Lernziel:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td">
            <div>
                <input onchange="changeCompetence()" class="radio-input" type="radio" id="radioRead" name="radios" value="read">
                <label class="radio-label" for="radioRead"></label>
                <?php echo form_label('Lesen', '', array('class' => 'form-label'));?>
                <input onchange="changeCompetence()" class="radio-input" type="radio" id="radioWrite" name="radios" value="write"checked>
                <label class="radio-label" for="radioWrite"></label>
                <?php echo form_label('Rechtschreibung', '', array('class' => 'form-label'));?>
            </div>
        </td>
    </tr>
    <tr>
        <td class="form-table-label-td">
            <?php echo form_label('Stufe:', '', array('class' => 'form-label'));?>
        </td>
        <td class="form-table-input-td">
            <!-- WRITE -->
            <div id="writeCompetenceLevels" style="display: block;">
                <select name="level-select-write" class="form-label elles-dropdown">
                    <?php if(isset($last_lesson) && ($last_lesson['cl_comp_id']==2)): ?>
                        <option value=<?php echo $last_lesson['cl_id']?>>
                            <?php echo 'Letzte Übung: '.$last_lesson['cl_name'].' '.$last_lesson['cl_remark']?>
                        </option>

                    <?php else: ?>
                        <option value="">Wähle Stufe</option>
                    <?php endif;?>

                    <?php if(count($all_comp_levels)):
                        foreach($all_comp_levels as $level):
                            if($level['comp_id'] == 2):?> <!-- Tabelle 'competences' id 2 = Lesen -->
                                <option value=<?php echo $level['id']?>>
                                    <?php echo $level['level'].': '.$level['name'].' '.$level['remark']?>
                                </option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <!-- READ -->
            <div id="readCompetenceLevels" style="display: none;">
                <select name="level-select-read" class="form-label elles-dropdown">
                    <?php if(isset($last_lesson) && ($last_lesson['cl_comp_id']==1)): ?>
                        <option value=<?php echo $last_lesson['cl_id']?>>
                            <?php echo 'Letzte Übung: '.$last_lesson['cl_name'].' '.$last_lesson['cl_remark']?>
                        </option>
                    <?php else: ?>
                        <option value="">Wähle Stufe</option>
                    <?php endif;?>

                    <?php if(count($all_comp_levels)):
                        foreach($all_comp_levels as $level):
                            if($level['comp_id'] == 1):?> <!-- Tabelle 'competences' id 1 = Lesen -->
                                <option value=<?php echo $level['id']?>>
                                    <?php echo $level['level'].': '.$level['name'].' '.$level['remark']?>
                                </option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </td>
    </tr>
    <!-- + EXERCISES -->
    <tr>
        <td class="form-table-label-td" style="vertical-align: top;">
            <?php echo form_label('Umsetzung:', '', array('class' => 'form-label'));?>
        </td>
        <td style="border: solid 1.5px gray;">
            <table style="width: 100%; padding: 12px 0 12px 0;">
                <col width="50%">
                <col width="50%">
                <tr>
                    <th style="text-align: left; padding-left: 15px">
                        <select name="ex-type-select" class="form-label" onchange="exTypeSelect(this.value);">
                            <?php if(isset($last_lesson)): ?>
                                <option value=<?php echo $last_lesson['et_id']?>>
                                    <?php echo 'Letzte Übung: '.$last_lesson['et_name']?>
                                </option>
                            <?php else: ?>
                                <option value="">
                                    Wähle Übung
                                </option>
                            <?php endif;?>

                            <?php if(count($all_ex_types)):
                                foreach($all_ex_types as $ex_type): ?>
                                    <option value= <?php echo $ex_type['id']?>><?php echo $ex_type['name']?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <option value="custom">
                                Benutzerdefiniert...
                            </option>
                        </select>
                        <div id="exTypeInputField" style="display: none;padding-top: 15px">
                            <?php echo form_input('new_ex_type',set_value('new_ex_type', ''),'placeholder="neue Übung eingeben"')?>
                        </div>
                        <br/>
                        <select name="ex-select" class="form-label" onchange="exSelect(this.value);">
                            <?php if(isset($last_lesson)): ?>
                                <option value=<?php echo $last_lesson['ex_id']?>>
                                    <?php echo 'Letzte Übung: '.$last_lesson['ex_name']?>
                                </option>
                            <?php else: ?>
                                <option value="">
                                    Wähle Übungsmaterial
                                </option>
                            <?php endif;?>

                            <?php if(count($all_ex)):
                                foreach($all_ex as $exercise): ?>
                                    <option value= <?php echo $exercise['id']?>><?php echo $exercise['name']?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <option value="custom">
                                Benutzerdefiniert...
                            </option>
                        </select>
                        <div id="exInputField" style="display: none; padding-top: 15px">
                            <?php echo form_input('new_ex', set_value('new_ex', ''),'placeholder="neues Material eingeben"')?>
                        </div>
                    </th>
                    <th style="text-align: center">
                        <?php
                        echo form_input('score', set_value('score', ''),'placeholder="" id="input-years"');
                        echo form_label(' / ');
                        echo form_input('max-score', set_value('max-score', ''),'placeholder="" id="input-years"');
                        echo form_label('Punkte');
                        ?>
                    </th>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="form-table-label-td" style="vertical-align: top">
            <?php echo form_label('Notizen:', '', array('class' => 'form-label'));?>
        <td>
            <?php echo form_textarea('notes',set_value('notes', ''),'placeholder= ""; id="input-textarea-full"');?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align: right">
            <?php echo form_submit('submit', 'Übung erstellen'); ?>
        </td>
    </tr>
</table>

<br/>


<script>
    function changeCompetence() {
        var readCompetenceLevels = document.getElementById('readCompetenceLevels');
        var writeCompetenceLevels = document.getElementById('writeCompetenceLevels');
        if(document.getElementById("radioRead").checked){
            readCompetenceLevels.style.display = 'block';
            writeCompetenceLevels.style.display = 'none';
        }
        if(document.getElementById("radioWrite").checked){
            readCompetenceLevels.style.display = 'none';
            writeCompetenceLevels.style.display = 'block';
        }
    }

    function exTypeSelect(value) {
        if(value == 'custom'){
            document.getElementById('exTypeInputField').style.display = 'block';
        } else {
            document.getElementById('exTypeInputField').style.display = 'none';
        }
    }

    function exSelect(value) {
        if(value == 'custom'){
            document.getElementById('exInputField').style.display = 'block';
        } else {
            document.getElementById('exInputField').style.display = 'none';
        }
    }




</script>


