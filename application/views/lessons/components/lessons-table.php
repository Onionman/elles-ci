<table class="info-table" align="center">
    <?php if(empty($all_lessons)): ?>
        <p class="gray">Noch keine Übungen!</p>
    <?php endif; ?>
    <?php
    $row_count = 0;
    foreach ($all_lessons as $lesson):
        ?>
        <tr class="table-tr">
            <td class="td-small">
                <?php echo date_format(new DateTime($lesson['date']), 'd.m.')?>
                <div class="form-label gray">
                    <i class="fa fa-clock-o"></i> <?php echo date_format(new DateTime($lesson['time']), 'G:i')?>
                </div>
            </td>
            <td>
                <?php if($lesson['cl_comp_id'] == 1): ?>
                    <img style="padding: 0" src="<?php echo base_url();?>webroot/img/book_small_new.png" alt="read_img"/>
                <?php else: ?>
                    <img style="padding: 0" src="<?php echo base_url();?>webroot/img/pen_small_new.png" alt="write_img"/>
                <?php endif; ?>
                <div class="form-label"><?php echo 'Stufe '. $lesson['cl_level'] ?></div>
            </td>
            <td class="td-small">
                <div class="form-label"><?php echo $lesson['cl_name'].': '. $lesson['cl_remark'] ?></div>
                <?php echo $lesson['et_name'];?>
                <br/>
                <div class="form-label gray"><?php echo $lesson['e_name'];?></div>
            </td>
            <td>
                <?php
                $percent = -1;
                $color = '';
                if($lesson['max_score'] == 0): //Fehlerbehandlung 'division by zero'
                    echo '0';
                else:
                    $percent = round($lesson['score']/$lesson['max_score']*100, 0);
                    if ($percent <= 20): $color = 'red';
                    elseif ($percent < 30 && $percent > 20): $color = 'orange';
                    elseif($percent > 70 && $percent < 90): $color = 'blue';
                    elseif ($percent >= 90): $color = 'green';
                    endif;
                endif;
                ?>
                <p style="color: <?php echo $color; ?>">
                    <?php echo $percent.' %'; ?>
                </p>
                <div class="form-label gray">
                    <?php echo $lesson['score'].'/'.$lesson['max_score'].' Punkte' ?>
                </div>
            </td>
            <td>
                <button class="link-button" type="button" id="extrainfo_<?php echo $row_count; ?>" onclick="showLessonExtraInfo(this.id)">
                    <i class="fa fa-chevron-down"></i>
                </button>
            </td>
            <td>
                <!-- ARCHIV DIALOG BOX  -->
                <button class="link-button" type="button" onclick="showDialog('<?php echo $lesson['l_id']; ?>')">
                <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
        <tr id="extrainfo_<?php echo $row_count; ?>_tr" style="display: none">
            <td colspan='2'>
                <div class="form-label" style="text-align: right">
                    Modus:
                </div>
                <?php if(!empty($lesson['origin'])): ?>
                <div class="form-label" style="text-align: right">
                    App:
                </div>
                <?php endif; ?>
                <div class="form-label" style="text-align: right">
                    Notizen:
                </div>
            </td>
            <td colspan='4'>
                <div class="form-label">
                    <?php echo $lesson['m_name']; ?>
                </div>
                <?php if(!empty($lesson['origin'])): ?>
                    <div class="form-label">
                        <?php echo $lesson['origin']?>
                    </div>
                <?php endif; ?>
                <div class="form-label">
                    <?php
                    if(!empty($lesson['notes'])):
                        echo $lesson['notes'];
                    else:
                        echo '-';
                    endif;
                    ?>
                </div>
            </td>
        </tr>
    <?php
    $row_count++;
    endforeach;
    ?>
</table>
<?php if (strlen($pagination)): ?>
    <div class="table-pagination">
        <?php echo $pagination; ?>
        <p class="unlinked-text">
            <?php echo $num_lessons. ' Einträge'; ?>
        </p>

    </div>
<?php endif; ?>

<div id="dialog-box">
    <div id="dialog-header">
        Diese Übung archivieren?
    </div>
    <div id="dialog-body">
        Übungen können nicht gelöscht werden. Bei der Archivierung
        sind Übungen nicht mehr sichtbar, bestehen jedoch noch im Archiv.
    </div>
    <div id="dialog-footer">
        <button id="dialog-button" type="button" onclick="dialogClose()">Abbrechen</button>
        <button id="dialog-button" type="button" onclick="dialogSubmit()">Ja</button>
    </div>
</div>

<script type="text/javascript">
    function showLessonExtraInfo(id)
    {
        var x = document.getElementById(id + "_tr");
        if (x.style.display === 'none')
        {
            x.style.display = '';
        }
        else
        {
            x.style.display = 'none';
        }
    }

    var lessonId = 0;
    
    function showDialog(id) {
        var blurBg = document.getElementById("blur-background");
        var dlgBox = document.getElementById("dialog-box");
        blurBg.style.display = "block";
        dlgBox.style.display = "block";
        dlgBox.style.left = window.innerWidth/2 - 480/2 + "px";
        dlgBox.style.top = "150px";
        lessonId = id;
    }

    function dialogClose() {
        var blurBg = document.getElementById("blur-background");
        var dlgBox = document.getElementById("dialog-box");
        blurBg.style.display = "none";
        dlgBox.style.display = "none";
    }

    function dialogSubmit() {
        var blurBg = document.getElementById("blur-background");
        var dlgBox = document.getElementById("dialog-box");
        blurBg.style.display = "none";
        dlgBox.style.display = "none";
        window.location.href = "<?php echo site_url('lessons/archive/');?>" + lessonId;
    }

</script>
