<table class="info-table" align="center">

    <?php foreach ($all_competences as $competence):

        ?>
        <tr class="table-tr">
            <td>
                <div class="form-label"><?php echo $competence['level']; ?></div>
            </td>
            <td style="text-align: center;">
                <div class="form-label"><?php echo $competence['cl_name']; ?></div>
            </td>
            <td style="text-align: center;">
                <div class="form-label"><?php echo $competence['remark']; ?></div>
            </td>
        </tr>
    <?php endforeach;?>
</table>