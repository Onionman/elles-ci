<?php $this->load->view('default/header'); ?>

<?php if(isset($_SESSION['message'])): ?>
    <div class="error">
        <?php echo $_SESSION['message'];?>
        <?php $this->session->unset_userdata('message'); ?>
    </div>
<?php endif; ?>

<table class="layout-table" align="center">
    <col width="15%" />
    <col width="35%" />
    <col width="35%" />
    <col width="15%" />
    <tr>
        <td></td>
        <td>
            <h3 style="text-align: left">Lernstufen</h3>
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr class="layout-table-td">
        <td></td>
        <td>
            <h3 style="text-align: left">Lesen</h3>
            <table class="info-table" align="center">
                <tr>
                    <td></td>
                    <td style="text-align: left">Stufe</td>
                    <td style="text-align: center; padding: 0">Anzahl</td>
                </tr>
                <?php foreach ($all_competences as $competence):
                    if($competence['comp_id'] == 1):
                    ?>
                    <tr class="table-tr">
                        <td>
                            <div class="form-label"><?php echo $competence['level']; ?></div>
                        </td>
                        <td style="text-align: left;">
                            <div class="form-label bold"><?php echo $competence['cl_name']; ?></div>
                            <div class="form-label"><?php echo $competence['remark']; ?></div>
                        </td>
                        <td style="text-align: center;">
                            <?php $has_count = FALSE; ?>
                            <?php foreach ($all_competences_count as $comp_count):
                                if ($competence['cl_id'] == $comp_count['cl_id']): ?>
                                    <div class="form-label">
                                        <?php $has_count = TRUE; ?>
                                        <?php echo $comp_count['count']; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if(!$has_count): ?>
                                <div class="form-label">0</div>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif;?>
                <?php endforeach;?>
            </table>
        </td>
        <td>
            <h3 style="text-align: left">Rechtschreibung</h3>
            <table class="info-table" align="center">
                <tr>
                    <td></td>
                    <td style="text-align: left">Stufe</td>
                    <td style="text-align: center; padding: 0">Anzahl</td>
                </tr>
                <?php foreach ($all_competences as $competence):
                    if($competence['comp_id'] == 2):
                    ?>
                    <tr class="table-tr">
                        <td>
                            <div class="form-label"><?php echo $competence['level']; ?></div>
                        </td>
                        <td style="text-align: left;">
                            <div class="form-label bold"><?php echo $competence['cl_name']; ?></div>
                            <div class="form-label"><?php echo $competence['remark']; ?></div>
                        </td>
                        <td style="text-align: center;">
                            <?php $has_count = FALSE; ?>
                            <?php foreach ($all_competences_count as $comp_count):
                                if ($competence['cl_id'] == $comp_count['cl_id']): ?>
                                    <div class="form-label">
                                        <?php $has_count = TRUE; ?>
                                        <?php echo $comp_count['count']; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if(!$has_count): ?>
                                <div class="form-label">0</div>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif;?>
                <?php endforeach;?>
            </table>
        </td>
        <td></td>
    </tr>
    <tr class="layout-table-td">
        <td></td>
        <td style="text-align: right">
            <h3 style="text-align: left">Übungen</h3>
            <div>
                <?php $this->load->view('exercises/components/exercise-types-table'); ?>
            </div>
        </td>
        <td>
            <h3 style="text-align: left">Übungsmaterial</h3>
            <div>
                <?php $this->load->view('exercises/components/exercises-table'); ?>
            </div>
        </td>
        <td></td>
    </tr>
    <tr class="layout-table-td">
        <td></td>
        <td style="text-align: right">
            <h3 style="text-align: left">Modi</h3>
            <div style="width: 80%">
                <canvas id="modesPieChart" width="200" height="200"></canvas>
            </div>
        </td>
        <td>
            <h3 style="text-align: left">Apps</h3>
            <div style="width: 80%">
                <canvas id="appsPieChart" width="200" height="200"></canvas>
            </div>
        </td>
        <td></td>
    </tr>
    <tr class="layout-table-td" style="display: none;">
        <td></td>
        <td colspan="2" style="text-align: right">
            <h3 style="text-align: left">Jahresansicht</h3>
            <div>
                <canvas id="lessonsBarChart" width="600" height="450"></canvas>
            </div>
        </td>
        <td></td>
    </tr>
</table>

<?php
echo "<script type='text/javascript'>\n";
echo "var modesChartData = " . json_encode($modes_chart_data) . ";\n";
echo "var appsChartData = " . json_encode($apps_chart_data) . ";\n";
//echo "var lessonsChartData = " . json_encode($lessons_chart_data) . ";\n";
echo "</script>\n";

//print json_encode($lessons_chart_data);
//print json_encode($modes_chart_data);
?>


<?php $this->load->view('default/footer'); ?>