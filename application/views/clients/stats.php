<?php $this->load->view('default/header'); ?>

<h2>Stats</h2>

<table class="layout-table" align="center">
    <tr>
        <td class="layout-table-td">
            <div>
                <?php $this->load->view('clients/components/client-profile'); ?>
            </div>
        </td>
        <td align="center">
            <div style="width: 95%;">
                <?php if(empty($chart_data_per_level)): ?>
                    <p class="gray">Noch keine Übungen!</p>
                <?php endif; ?>
                <canvas id="barChart" width="600" height="400"></canvas>
            </div>
        </td>
        <td class="layout-table-td">
            <div>
                <?php $this->load->view('clients/components/client-navigation'); ?>
            </div>
        </td>
    </tr>
</table>

<?php
echo "<script type='text/javascript'>\n";
echo "var chartDataPerLevel = " . json_encode($chart_data_per_level) . ";\n";
echo "var clientCreated = " . json_encode(($client['created'])) . ";\n";
echo "var lastLessonDate = " . json_encode(($last_lesson['date'])) . ";\n";
echo "</script>\n";
?>

<?php //print json_encode($chart_data_per_level); ?>


<?php $this->load->view('default/footer'); ?>


