<?php $this->load->view('default/header'); ?>

<?php if(isset($_SESSION['message'])): ?>
    <div class="message">
        <?php echo $_SESSION['message'];?>
        <?php $this->session->unset_userdata('message'); ?>
    </div>
<?php endif; ?>

<h2>Übungen</h2>

<?php echo validation_errors(); ?>

<table class="layout-table" align="center">
    <col width="15%" />
    <col width="70%" />
    <col width="15%" />
    <tr>
        <td class="layout-table-td">
            <div>
                <?php $this->load->view('clients/components/client-profile'); ?>
            </div>
        </td>
        <td class="layout-table-td">
            <div style="text-align: right">
                <button onclick="showLessonCreator()" type="button" style="horiz-align: right">
                    <i class="fa fa-chevron-down"></i> neue Übung</button>
            </div>
            <div>
                <?php if(isset($lesson_creater_error)): ?>
                    <div id="lessonCreator" style="display: block">
                        <?php else: ?>
                    <div id="lessonCreator" style="display: none">
                        <?php endif; ?>
                    <?php $this->load->view('lessons/components/lesson-create'); ?>
                </div>
            </div>
           <div>
               <?php $this->load->view('lessons/components/lessons-table'); ?>
           </div>
        </td>
        <td class="layout-table-td">
            <div>
                <?php $this->load->view('clients/components/client-navigation'); ?>
            </div>
        </td>
    </tr>
</table>

<script>
    function showLessonCreator()
    {
        var x = document.getElementById('lessonCreator');
        if (x.style.display === 'none')
        {
            x.style.display = 'block';
        }
        else
        {
            x.style.display = 'none';
        }
    }
</script>

<?php $this->load->view('default/footer'); ?>



