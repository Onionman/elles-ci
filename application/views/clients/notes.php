<?php $this->load->view('default/header'); ?>

<h2>Notizen</h2>

<table class="layout-table" align="center">
    <tr>
        <td class="layout-table-td">
            <div>
                <?php $this->load->view('clients/components/client-profile'); ?>
            </div>
        </td>
        <td class="layout-table-td">
            <div>
                <?php
                echo form_open('clients/notes/'.$client['id']);
                echo form_textarea('notes', $client['notes'],'id="input-textarea-full" style="height: 400px;"');
                ?>

            </div>
            <div align="right">
                <?php
                echo form_submit('submit', 'Speichern');
                echo form_close();
                ?>
            </div>
        </td>
        <td class="layout-table-td">
            <div>
                <?php $this->load->view('clients/components/client-navigation'); ?>
            </div>
        </td>
    </tr>
</table>

<?php $this->load->view('default/footer'); ?>