<?php $this->load->view('default/header'); ?>

<h2>Patient hinzufügen</h2>

<?php echo form_open('clients/create'); ?>
<?php echo validation_errors(); ?>

<table class="layout-table" align="center">
    <col width="25%" />
    <col width="40%" />
    <col width="25%" />
    <tr>
        <td class="layout-table-td">
            <div style="padding: 30px;">
                <div id="genderImageBoy" style="display: block; width: 100%; text-align: center">
                    <img src="<?php echo base_url();?>webroot/img/boy.png" alt="boy image"/>
                </div>
                <div id="genderImageGirl" style="display: none">
                    <img src="<?php echo base_url();?>webroot/img/girl.png" alt="boy image"/>
                </div>
                <div>
                    <input onchange="changeGenderImage()" class="radio-input" type="radio" id="radioBoy" name="gender" checked value="male">
                    <label class="radio-label" for="radioBoy"></label>
                    <?php echo form_label('Männlich', '', array('class' => 'form-label'));?>
                    <input onchange="changeGenderImage()" class="radio-input" type="radio" id="radioGirl" name="gender" value="female">
                    <label class="radio-label" for="radioGirl"></label>
                    <?php echo form_label('Weiblich', '', array('class' => 'form-label'));?>
                </div>
            </div>
        </td>
        <td>
            <table class="form-table">
                <col width="30%" />
                <col width="70%" />
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Nachname:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('lastname', set_value('lastname', ''), 'placeholder=""');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Vorname:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('firstname', set_value('firstname', ''), 'placeholder=""');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Geburtsdatum:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php
                        echo form_input('day', set_value('day', ''), 'placeholder="TT" id="input-days"');
                        echo form_label('/');
                        echo form_input('month', set_value('month', ''), 'placeholder="MM" id="input-days"');
                        echo form_label('/');
                        echo form_input('year', set_value('year', ''), 'placeholder="JJJJ" id="input-years"');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Adresse:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('street', set_value('street', ''), 'placeholder="(optional)"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('PLZ / Ort:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php
                        echo form_input('zip_code', set_value('zip_code', ''), 'placeholder="(optional)" id="input-zip-code"');
                        echo ' / ';
                        echo form_input('location', set_value('location', ''), 'placeholder="(optional)" id="input-city"');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Telefon:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('phone', set_value('phone', ''), 'placeholder="(optional)"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Mobiltelefon:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('phone_mobile',  set_value('phone_mobile', ''), 'placeholder="(optional)"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('E-mail:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('email' , set_value('email', ''), 'placeholder="(optional)"'); ?>
                    </td>
                </tr>

                <tr>
                    <td class="form-table-label-td"></td>
                    <td class="form-table-input-td">

                    </td>
                </tr>
            </table>
        </td>
        <td class="layout-table-td" style="text-align: right">
            <div style="padding: 0 50px 0 20px">
                <?php echo form_textarea('notes' , set_value('notes', ''), 'placeholder="Notizen" id="input-textarea"');?>
                <br/>
                <?php
                echo form_submit('submit', 'erstellen');
                echo form_close();
                ?>
            </div>
        </td>
    </tr>
</table>

<script>
    function changeGenderImage() {
        var boyImg = document.getElementById('genderImageBoy');
        var girlImg = document.getElementById('genderImageGirl');
        var radioBoy = document.getElementById('radioBoy');
        var radioGirl = document.getElementById('radioGirl');
        if(document.getElementById("radioBoy").checked){
            boyImg.style.display = 'block';
            girlImg.style.display = 'none';
        }
        if(document.getElementById("radioGirl").checked){
            boyImg.style.display = 'none';
            girlImg.style.display = 'block';
        }
    }
</script>

<?php $this->load->view('default/footer'); ?>

