<?php echo form_open('clients/create'); ?>
<?php echo validation_errors(); ?>

<table class="layout-table" align="center">
    <col width="56%" />
    <col width="44%" />
    <tr>
        <td class="layout-table-td">
            <table class="form-table">
                <col width="30%" />
                <col width="70%" />
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Nachname:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('lastname', set_value('lastname', ''), 'placeholder=""');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Vorname:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('firstname', set_value('firstname', ''), 'placeholder=""');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Geburtsdatum:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php
                        echo form_input('day', set_value('day', ''), 'placeholder="TT" id="input-days"');
                        echo form_label('/');
                        echo form_input('month', set_value('month', ''), 'placeholder="MM" id="input-days"');
                        echo form_label('/');
                        echo form_input('year', set_value('year', ''), 'placeholder="JJJJ" id="input-years"');
                        ?>
                    </td>
                </tr>
            </table>
        </td>
        <td class="layout-table-td">
            <div style="margin: 15px 0 15px 0;">
                <div id="genderImageBoy" style="display: block; width: 100%; text-align: center">
                    <img src="<?php echo base_url();?>webroot/img/boy.png" alt="boy image"/>
                </div>
                <div id="genderImageGirl" style="display: none">
                    <img src="<?php echo base_url();?>webroot/img/girl.png" alt="boy image"/>
                </div>
                <div>
                    <input onchange="changeGenderImage()" class="radio-input" type="radio" id="radioBoy" name="gender" checked value="male">
                    <label class="radio-label" for="radioBoy"></label>
                    <?php echo form_label('Männlich', '', array('class' => 'form-label'));?>
                    <input onchange="changeGenderImage()" class="radio-input" type="radio" id="radioGirl" name="gender" value="female">
                    <label class="radio-label" for="radioGirl"></label>
                    <?php echo form_label('Weiblich', '', array('class' => 'form-label'));?>
                </div>
            </div>
            <div style="width: 100%; text-align: right">
                <?php
                echo form_submit('submit', 'erstellen');
                echo form_close();
                ?>
            </div>
        </td>
    </tr>
</table>