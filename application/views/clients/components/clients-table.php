<table class="info-table" align="center">
    <?php foreach ($clients as $client):

        $no_lessons = TRUE;
        ?>
        <tr class="table-tr">
            <td>
                <?php if($client['is_active'] == "t"):?>
                    <?php echo $client['lastname']; ?>
                <?php else: ?>
                    <div class="gray"><?php echo $client['lastname']; ?></div>
                <?php endif; ?>
                <div class="form-label gray"><?php echo $client['firstname']; ?></div>
            </td>
            <td style="text-align: center">
                <?php foreach ($num_lessons_per_client as $num_lessons): ?>
                    <?php if($client['id'] == $num_lessons['client_id']):
                            $no_lessons = FALSE;
                            echo $num_lessons['num_lessons'];
                            if($num_lessons['num_lessons'] == 1): ?>
                                <div class="form-label">Übung</div>
                            <?php else: ?>
                                <div class="form-label">Übungen</div>
                            <?php endif;?>
                    <?php endif;?>
                <?php endforeach;?>
                <?php if($no_lessons): ?>
                    <div class="form-label gray">noch keine Übungen</div>
                <?php endif; ?>
            </td>
            <td style="text-align: center">
                <?php if($client['is_active'] == "t"):?>
                    <?php if(!empty($client['modified'])): ?>
                        <?php
                        $day_difference = date_diff(date_create($client['modified']), date_create('today'))->format('%a');
                        $month_difference = date_diff(date_create($client['modified']), date_create('today'))->format('%m');
                        ?>
                        <p class="unlinked-text">
                            Letzte Aktion:
                        </p>
                        <p class="unlinked-text">

                        <?php
                        if ($month_difference == 0):
                            if($day_difference == 0): echo 'Heute';
                            elseif($day_difference == 1): echo 'Gestern';
                            elseif($day_difference == 2): echo 'Vorgestern';
                            else: echo 'vor '.$day_difference.' Tagen.';
                            endif;
                        elseif ($month_difference == 1):
                            echo 'vor '.$month_difference.' Monat';
                        else:
                            echo 'vor '.$month_difference.' Monaten';
                        endif;
                        ?>
                        <?php endif; ?>
                <?php else: ?>
                    <p class="unlinked-text">
                        Deaktiviert
                    </p>
                <?php endif; ?>
            </td>
            <td>
                <a href="<?php echo site_url('clients/view/'.$client['c_id']); ?>" class="table-action-a">
                    <i class="fa fa-eye"></i>
                </a>
            </td>
            <td>
                <a href="<?php echo site_url('clients/edit/'.$client['c_id']); ?>" class="table-action-a">
                    <i class="fa fa-pencil-square-o"></i></a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
