<table class="table-side" align="right">
    <tr>
        <td style="text-align: right">
            <div style="padding: 0 0 10px 0">
                <a href="<?php echo site_url('clients/stats/'.$client['id']); ?>">
                    <i class="fa fa-line-chart"></i> Statistik</a>
            </div>
            <div style="padding: 0 0 10px 0">
                <a href="<?php echo site_url('clients/view/'.$client['id']); ?>">
                    <i class="fa fa-book"></i> Übungen</a>
            </div>
            <div style="padding: 0 0 10px 0">
                <a href="<?php echo site_url('clients/notes/'.$client['id']); ?>">
                    <i class="fa fa-address-card-o"></i> Notizen</a>
            </div>
        </td>
    </tr>
</table>