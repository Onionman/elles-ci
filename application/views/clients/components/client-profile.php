<table class="component left">
    <tr>
        <td>
            <?php if($client['gender'] == 'male'): ?>
                <img src="<?php echo base_url();?>webroot/img/boy.png" alt="boy image"/>
            <?php else: ?>
                <img src="<?php echo base_url();?>webroot/img/girl.png" alt="girl image"/>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $client['lastname']. ' '. $client['firstname']; ?>
                <p class="unlinked-text">Alter:
                    <?php echo date_diff(date_create($client['birthday']), date_create('today'))->y; ?>
                </p>
            <p class="form-label">Kürzel: <?php echo $client['acronym'] ?></p>
        </td>
    </tr>
    <tr>
        <td>
            <div class="dropdown">
                <button class="button-small"><i class="fa fa-caret-square-o-down"></i> persönliche Daten</button>
                <div class="dropdown-content-info">
                    <table class="form-table">
                        <tr>
                            <td class="form-table-label-td" style="vertical-align: top">
                                <i class="fa fa-home"></i>
                            </td>
                            <td class="form-table-input-td">
                                <p class="unlinked-text">
                                    <?php echo (empty($client['street'])) ? '-' : $client['street']; ?>
                                    <br/>
                                    <?php echo (empty($client['zip_code'])) ? '-' : $client['zip_code'].' '; ?>
                                    <?php echo (empty($client['location'])) ? '-' : $client['location']; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="form-table-label-td">
                                <i class="fa fa-phone"></i>
                            </td>
                            <td class="form-table-input-td">
                                <p class="unlinked-text">
                                    <?php echo (empty($client['phone'])) ? '-' : $client['phone']; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="form-table-label-td">
                                <i class="fa fa-mobile"></i>
                            </td>
                            <td class="form-table-input-td">
                                <p class="unlinked-text">
                                    <?php echo (empty($client['phone_mobile'])) ? '-' : $client['phone_mobile']; ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="form-table-label-td">
                                <i class="fa fa-birthday-cake"></i>
                            </td>
                            <td class="form-table-input-td">
                                <p class="unlinked-text">
                                    <?php echo (empty($client['birthday'])) ? '-' : date_format(new DateTime($client['birthday']), 'd.m.Y'); ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="form-table-label-td"></td>
                            <td class="form-table-input-td" style="text-align: right">
                                <p class="unlinked-text"><a href="<?php echo site_url('clients/edit/'.$client['c_id']); ?>">
                                        <i class="fa fa-pencil-square-o"></i> bearbeiten
                                    </a></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <br/>
            <?php if(isset($last_lesson)):
                $date = new DateTime($last_lesson['date'].' '.$last_lesson['time']);
                $unix_timestamp = date_timestamp_get($date);
                $now = time();
                $units = 1;
                ?>

            <p class="unlinked-text">
                <?php
                echo 'Letzte Übung vor: ';
                echo '<br/>';
                echo timespan($unix_timestamp, $now, $units);?>
            </p>
            <?php endif; ?>

            <br/>
            <p class="unlinked-text"><?php echo 'Erstellt am '.date_format(new DateTime($client['created']), 'd.m.Y'); ?></p>

        </td>
    </tr>
</table>