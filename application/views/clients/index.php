<?php $this->load->view('default/header'); ?>


<?php if(isset($_SESSION['message'])): ?>
    <div class="error">
            <?php echo $_SESSION['message'];?>
            <?php $this->session->unset_userdata('message'); ?>
    </div>
<?php endif; ?>

<table class="layout-table" align="center">
    <col width="20%" />
    <col width="60%" />
    <col width="20%" />
    <tr>
        <td></td>
        <td>
            <h3 style="text-align: left">Heute</h3>
            <div>
                <?php $this->load->view('lessons/components/lesson-dashboard'); ?>
            </div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="text-align: right">
            <h3 style="text-align: left">Klienten</h3>
            <?php
            echo form_open('clients/index');
            echo form_input('search', $search_string, 'placeholder="Vor-/Nachname eingeben" class="input-search"');
            echo form_submit('submit', 'Suchen');
            echo form_close();
            ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="layout-table-td">
        </td>
        <td>
            <div>
                <?php $this->load->view('clients/components/clients-table'); ?>
            </div>
            <table style="width: 100%">
                <col width="30%" />
                <col width="40%" />
                <col width="30%" />
                <tr>
                    <td></td>
                    <td>
                        <?php if(empty($search_string)):?>
                            <?php if (strlen($pagination)): ?>
                                <div class="table-pagination">
                                    <?php
                                    echo $pagination;
                                    ?><p class="unlinked-text"><?php echo $num_clients. ' Einträge';
                                        ?></p>

                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <div style="text-align: right">
                            <button onclick="showClientCreator()" type="button" style="horiz-align: right">
                                <i class="fa fa-user-plus"></i> neuer Klient</button>
                        </div>
                    </td>
                </tr>
            </table>
            <?php if(isset($client_creater_error)): ?>
            <div id="clientCreator" style="display: block">
                <?php else: ?>
                <div id="clientCreator" style="display: none">
                    <?php endif; ?>
                    <?php $this->load->view('clients/components/client-creator'); ?>
                </div>

            </div>
        </td>
        <td class="layout-table-td">
            <div style="display: none">
                Filter:
                <select name="level-select-write" class="form-label elles-dropdown">
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                </select>
                <select name="level-select-write" class="form-label elles-dropdown">
                    <option value="A" >Aktive Klienten anzeigen</option>
                    <option value="B">Inaktive Klienten anzeigen</option>
                </select>
            </div>
        </td>
    </tr>
</table>

<script>
    function showClientCreator() {
        var x = document.getElementById('clientCreator');
        if (x.style.display === 'none')
        {
            x.style.display = 'block';
        }
        else
        {
            x.style.display = 'none';
        }
    }

    function changeGenderImage() {
        var boyImg = document.getElementById('genderImageBoy');
        var girlImg = document.getElementById('genderImageGirl');
        var radioBoy = document.getElementById('radioBoy');
        var radioGirl = document.getElementById('radioGirl');
        if(document.getElementById("radioBoy").checked){
            boyImg.style.display = 'block';
            girlImg.style.display = 'none';
        }
        if(document.getElementById("radioGirl").checked){
            boyImg.style.display = 'none';
            girlImg.style.display = 'block';
        }
    }
</script>

<?php $this->load->view('default/footer'); ?>

