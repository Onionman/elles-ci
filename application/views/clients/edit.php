<?php $this->load->view('default/header'); ?>

<h2>Klient bearbeiten</h2>

<table class="layout-table">
    <col width="25%" />
    <col width="50%" />
    <col width="25%" />
    <tr>
        <td>

        </td>
        <td class="layout-table-td">

            <?php
            echo form_open('clients/edit/'.$client['id']);
            echo validation_errors();
            ?>
            <table class="form-table" align="center">
                <col width="32%"/>
                <col width="68%"/>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Nachname:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('lastname', $client['lastname'], 'placeholder="(optional)"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Vorname:', '', array('class' => 'form-label')); ?>
                    </td>
                    <td class="form-table-input-td"">
                    <?php echo form_input('firstname', $client['firstname'], 'placeholder="Vorname" id="form-table-input"'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Geburtsdatum:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php
                        $day = date_format(new DateTime($client['birthday']), 'd');
                        $month = date_format(new DateTime($client['birthday']), 'm');
                        $year = date_format(new DateTime($client['birthday']), 'Y');
                        echo form_input('day', $day, 'placeholder="TT" id="input-days"');
                        echo form_label('/');
                        echo form_input('month', $month, 'placeholder="MM" id="input-days"');
                        echo form_label('/');
                        echo form_input('year', $year, 'placeholder="JJJJ" id="input-years"');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Geschlecht:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <div>
                            <input onchange="changeGenderImage()"
                                   class="radio-input" type="radio" id="radioBoy" name="gender" value="male"
                                    <?php echo ($client['gender']=='male')?'checked':''?>>
                            <label class="radio-label" for="radioBoy"></label>
                            <?php echo form_label('Männlich', '', array('class' => 'form-label'));?>
                            <input onchange="changeGenderImage()"
                                   class="radio-input" type="radio" id="radioGirl" name="gender" value="female"
                            <?php echo ($client['gender']=='female')?'checked':''?>>
                            <label class="radio-label" for="radioGirl"></label>
                            <?php echo form_label('Weiblich', '', array('class' => 'form-label'));?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Strasse:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('street', $client['street'], 'placeholder="(optional)" id="form-table-input"'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('PLZ:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('zip_code', $client['zip_code'], 'placeholder="(optional)" id="form-table-input"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Ort:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('location', $client['location'], 'placeholder="(optional)" id="form-table-input"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Telefonnummer:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('phone', $client['phone'], 'placeholder="(optional)" id="form-table-input"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Mobilnummer:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php echo form_input('phone_mobile', $client['phone_mobile'], 'placeholder="(optional)" id="form-table-input"');?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Erstellungsdatum:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <?php
                        $day = date_format(new DateTime($client['created']), 'd');
                        $month = date_format(new DateTime($client['created']), 'm');
                        $year = date_format(new DateTime($client['created']), 'Y');
                        echo form_input('created_day', $day, 'placeholder="TT" id="input-days"');
                        echo form_label('/');
                        echo form_input('created_month', $month, 'placeholder="MM" id="input-days"');
                        echo form_label('/');
                        echo form_input('created_year', $year, 'placeholder="JJJJ" id="input-years"');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                        <?php echo form_label('Aktiv:', '', array('class' => 'form-label'));?>
                    </td>
                    <td class="form-table-input-td">
                        <div>
                            <input class="radio-input" type="radio" id="radioActive" name="active" value="yes"
                                <?php echo ($client['is_active']=="t")?'checked':''?>>
                            <label class="radio-label" for="radioActive"></label>
                            <?php echo form_label('Ja', '', array('class' => 'form-label'));?>
                            <input class="radio-input" type="radio" id="radioInactive" name="active" value="no"
                                <?php echo ($client['is_active']=="f")?'checked':''?>>
                            <label class="radio-label" for="radioInactive"></label>
                            <?php echo form_label('Nein', '', array('class' => 'form-label'));?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="form-table-input-td">
                        <button class="link-button" type="button" onclick="showDialog(<?php echo $client['id']; ?>)">
                            <?php echo $client['lastname'].' '.$client['firstname'] ?> löschen?
                        </button>
                    </td>
                </tr>
                <tr>
                    <td class="form-table-label-td">
                    </td>
                    <td class="form-table-input-td">
                        <?php
                        echo form_submit('submit', 'Speichern');
                        echo form_close();
                        ?>

                    </td>
                </tr>
            </table>
        </td>
        <td>

        </td>
    </tr>
</table>

<div id="dialog-box">
    <div id="dialog-header">
        Diesen Klient wirklich löschen?
    </div>
    <div id="dialog-body">
        Diese Aktion kann nicht mehr rückgängig gemacht werden.
    </div>
    <div id="dialog-footer">
        <button id="dialog-button" type="button" onclick="dialogClose()">Abbrechen</button>
        <button id="dialog-button" type="button" onclick="dialogSubmit()">Ja</button>
    </div>
</div>

<script type="text/javascript">
    var clientId = 0;

    function showDialog(id) {
        var blurBg = document.getElementById("blur-background");
        var dlgBox = document.getElementById("dialog-box");
        blurBg.style.display = "block";
        dlgBox.style.display = "block";
        dlgBox.style.left = window.innerWidth/2 - 480/2 + "px";
        dlgBox.style.top = "150px";
        clientId = id;
    }

    function dialogClose() {
        var blurBg = document.getElementById("blur-background");
        var dlgBox = document.getElementById("dialog-box");
        blurBg.style.display = "none";
        dlgBox.style.display = "none";
    }

    function dialogSubmit() {
        var blurBg = document.getElementById("blur-background");
        var dlgBox = document.getElementById("dialog-box");
        blurBg.style.display = "none";
        dlgBox.style.display = "none";
        window.location.href = "<?php echo site_url('clients/delete/');?>" + clientId;
    }

</script>

<?php $this->load->view('default/footer'); ?>