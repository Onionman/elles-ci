<?php $this->load->view('default/header_guest'); ?>

<div>
    <?php if (isset($account_created)) {?><h3><?php echo $account_created; ?></h3>
    <?php } else if (isset($validate_fail)) { ?><h3><?php echo $validate_fail; ?></h3>
    <?php } else { ?><h2>Bitte anmelden.</h2><?php } ?>

    <?php echo form_open('login/validate_credentials');?>

    <br/>
    <?php echo form_input('email', '', 'placeholder="Email"');?>
    <br/>
    <br/>
    <?php echo form_password('password', '', 'placeholder="Passwort"');?>
    <br/>
    <br/>
    <?php echo form_submit('submit', 'Login'); ?>
    <a href="<?php echo site_url('login/register'); ?>" class="unlinked-text">noch nicht registriert?</a>

    <?php echo form_close(); ?>
</div>

<?php $this->load->view('default/footer'); ?>