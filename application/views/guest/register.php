<?php $this->load->view('default/header_guest'); ?>

<div>
    <?php if (isset($account_not_created)) { ?>
        <h3><?php echo $account_not_created; ?></h3>
    <?php } ?>
    <h2>Registrieren</h2>

    <?php echo form_open('login/register');?>
    <br/>
    <?php echo form_input('email' , set_value('email', ''), 'placeholder="Email"'); ?>
    <br/>
    <br/>
    <?php echo form_password('password' , '', 'placeholder="Passwort"'); ?>
    <br/>
    <br/>
    <?php echo form_password('password_confirm' , '', 'placeholder="Passwort Bestätigen" class="password"'); ?>
    <br/>
    <br/>
    <?php echo form_submit('submit', 'Registrieren');?>
    <a href="<?php echo site_url('login/login_user'); ?>" class="unlinked-text">schon registriert?</a>
    <?php echo validation_errors('<p class="error">')?>
</div>

<?php $this->load->view('default/footer'); ?>