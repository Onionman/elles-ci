<?php $this->load->view('default/header_guest'); ?>

<h2>Willkommen!</h2>

<div style="padding: 5% 15% 0 15%">
    <div style="overflow: auto; padding: 30px;" >
        <div style="float: left;">
            <img style="display: block" src="<?php echo base_url();?>webroot/img/clients.png" alt="clients_img"/>
        </div>
        <div style="float: left; display: none">
            <span>
                <i class="fa fa-users fa-4x"></i>
            </span>
        </div>

        <div style="margin-top: 30px; margin-left: 200px; padding: 10px; text-align: left;">
            Erstellen und verwalten Sie ihre Patienten um eine schnelle Übersicht zu erhalten.
        </div>
    </div>

    <div style="overflow: auto; padding: 30px" >
        <div style="float: right">
            <img style="display: block" src="<?php echo base_url();?>webroot/img/exercises.png" alt="clients_img"/>
        </div>
        <div style="float: right; display: none">
            <span>
                <i class="fa fa-book fa-4x"></i>
            </span>
        </div>

        <div style="margin-top: 20px; margin-right: 200px; padding: 10px; text-align: left;">
            Protokollieren Sie Übungen, die Sie mit den Patienten durchführen. Nutzen Sie dazu Apps oder tragen Sie Übungen, die analog gemacht wurden mit wenigen Klicks ein.
        </div>
    </div>

    <div style="overflow: auto; padding: 30px" >
        <div style="float: left">
            <img style="display: block" src="<?php echo base_url();?>webroot/img/charts.png" alt="clients_img"/>
        </div>
        <div style="float: left; display: none">
            <span>
                <i class="fa fa-line-chart fa-4x"></i>
            </span>
        </div>

        <div style="margin-top: 30px; margin-left: 200px; padding: 10px; text-align: left">
            Behalten Sie den Lernfortschritt ihrer Patienten durch anschauliche Grafiken im Auge.
        </div>
    </div>

    <div style="overflow: auto; padding: 30px" >
        <div style="margin-top: 30px; margin-left: 0px; padding: 10px; text-align: center; font-weight: bold">
            Erstellen Sie kostenlos Ihr eigenes Benutzerprofil mit Ihren eigenen Zugangsdaten.
            Klicken Sie dazu auf <a href="<?php echo site_url('login/register'); ?>" id="">Registrieren</a>.
        </div>
    </div>
</div>


<?php $this->load->view('default/footer'); ?>