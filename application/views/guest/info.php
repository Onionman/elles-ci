<?php $this->load->view('default/header_guest'); ?>

<p>
    Mit Elles kannst du:
    </br></br>
    1) Patienten erstellen, verwalten und eine schnelle Übersicht erhalten
    2) Übungen eintragen
    3) Schnell und einfach einen aktuellen Lernfortschritt des Patienten einsehen

</p>

<?php $this->load->view('default/footer'); ?>